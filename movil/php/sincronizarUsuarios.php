<?php
  include("../../php/conectar.php");

  $link = Conectar();
  $Fecha = $_POST['Fecha'];
  //$Fecha = "2015-01-01 00:00:00";

   $sql = "SELECT 
               Login.idLogin AS 'idLogin',
               Login.Usuario AS 'Usuario',
               DatosUsuarios.Nombre AS 'Nombre',
               Login.Clave AS 'Clave',
               DatosUsuarios.foto AS 'Imagen',
               Login.Tipo AS 'Tipo'
            FROM 
               Login INNER JOIN DatosUsuarios ON Login.idLogin = DatosUsuarios.idLogin
            WHERE 
               Fecha > '" . $Fecha . "'
               AND Login.Estado = 'Activo';";

  $result = $link->query($sql);

  if ( $result->num_rows > 0)
  {
    class User
        {
           public $idLogin;
           public $Usuario;
           public $Nombre;
           public $Clave;
           public $Imagen;
           public $Tipo;
        }

    $idx = 0;
     while ($row = mysqli_fetch_assoc($result))
     { 
        $Usuarios[$idx] = new User();
        $Usuarios[$idx]->idLogin = utf8_encode($row['idLogin']);
        $Usuarios[$idx]->Usuario = utf8_encode($row['Usuario']);
        $Usuarios[$idx]->Nombre = utf8_encode($row['Nombre']);
        $Usuarios[$idx]->Clave = utf8_encode($row['Clave']);
        $Usuarios[$idx]->Imagen = utf8_encode($row['Imagen']);
        $Usuarios[$idx]->Tipo = utf8_encode($row['Tipo']);

        $idx++;
     }
     
        mysqli_free_result($result);  
        echo json_encode($Usuarios);
  } else
  {
    echo 0;
  }   

?>