<?php
  include("../../php/conectar.php");

  $link = Conectar();
  $Fecha = $_POST['Fecha'];
  //$Fecha = "2015-01-01 00:00:00";
  

   $sql = "SELECT 
            Beneficiarios.idBeneficiario,
            Beneficiarios.Nombre,
            Beneficiarios.Documento,
            Beneficiarios.Direccion,
            Beneficiarios.Barrio,
            Beneficiarios.Telefono,
            Beneficiarios.Celular,
            Beneficiarios.Estrato,
            CodDane_Departamentos.Departamento,
            CodDane_Municipios.NomMunicipio,
            Beneficiarios.Correo,
            Beneficiarios.Genero,
            Beneficiarios.Contrato,
            Beneficiarios.Estado
          FROM Beneficiarios
            INNER JOIN CodDane_Departamentos ON CodDane_Departamentos.Codigo = Beneficiarios.Departamento
            INNER JOIN CodDane_Municipios ON CodDane_Municipios.CodMunicipio = Beneficiarios.Municipio AND CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento
          WHERE 
               Beneficiarios.Estado = 'Programado';";

  $result = $link->query($sql);

  if ( $result->num_rows > 0)
  {
    class Beneficiario
        {
          public $idBeneficiario;
          public $Nombre;
          public $Documento;
          public $Direccion;
          public $Barrio;
          public $Telefono;
          public $Celular;
          public $Estrato;
          public $Departamento;
          public $Municipio;
          public $Correo;
          public $Genero;
          public $Contrato;
          public $Estado;
        }

    $idx = 0;
     while ($row = mysqli_fetch_assoc($result))
     { 
        $Beneficiarios[$idx] = new Beneficiario();
        $Beneficiarios[$idx]->idBeneficiario = utf8_encode($row['idBeneficiario']);
        $Beneficiarios[$idx]->Nombre = utf8_encode($row['Nombre']);
        $Beneficiarios[$idx]->Documento = utf8_encode($row['Documento']);
        $Beneficiarios[$idx]->Direccion = utf8_encode($row['Direccion']);
        $Beneficiarios[$idx]->Barrio = utf8_encode($row['Barrio']);
        $Beneficiarios[$idx]->Telefono = utf8_encode($row['Telefono']);
        $Beneficiarios[$idx]->Celular = utf8_encode($row['Celular']);
        $Beneficiarios[$idx]->Estrato = utf8_encode($row['Estrato']);
        $Beneficiarios[$idx]->Departamento = utf8_encode($row['Departamento']);
        $Beneficiarios[$idx]->Municipio = utf8_encode($row['NomMunicipio']);
        $Beneficiarios[$idx]->Correo = utf8_encode($row['Correo']);
        $Beneficiarios[$idx]->Genero = utf8_encode($row['Genero']);
        $Beneficiarios[$idx]->Contrato = $row['Contrato'];
        $Beneficiarios[$idx]->Estado = $row['Estado'];

        $idx++;
     }
     
        mysqli_free_result($result);  
        echo json_encode($Beneficiarios);
  } else
  {
    echo 0;
  }   

?>
