<?php
	if(!extension_loaded("soap"))
	{
	      dl("php_soap.dll");
	}

	include("../php/conectar.php");
	
	 
	ini_set("soap.wsdl_cache_enabled","0");
	$server = new SoapServer("ServiciosCompartel.wsdl", array('encoding'=>'UTF-8'));
	 
	function ObtenerNivelSatisfaccionUsuario($IdInterventor,$MarcaTiempo)
	{
		$MarcaTiempo = str_replace("T", " ", $MarcaTiempo);
	      $sql = "SELECT * FROM WS_NSU WHERE IdInterventor = '$IdInterventor' AND MarcaTiempo >= '$MarcaTiempo'";
	      $link = Conectar();
	      $result = $link->query($sql);
			

		   if ( $result->num_rows > 0)
		   {
         	$idx = 0;
		   	while ($row = mysqli_fetch_assoc($result))
         	{
				$ArrayOfNivelSatisfaccionUsuario[] = array(
					'IdInterventor'  => $row['IdInterventor'],
					'NumeroContrato'  => $row['NumeroContrato'],
					'Ano'  => $row['Ano'],
					'DaneDepartamento'  => $row['DaneDepartamento'],
					'DaneMunicipio'  => $row['DaneMunicipio'],
					'DaneInstitucion'  => $row['DaneInstitucion'],
					'DaneSede'  => $row['DaneSede'],
					'CodigoSimona'  => $row['CodigoSimona'],
					'NumeroReferenciaPago'  => $row['NumeroReferenciaPago'],
					'CodigoTipoFormulario'  => $row['CodigoTipoFormulario'],
					'VersionTipoFormulario'  => $row['VersionTipoFormulario'],
					'IdFormulario'  => $row['IdFormulario'],
					'FechaEncuesta'  => $row['FechaEncuesta'],
					'Pregunta'  => utf8_encode($row['Pregunta']),
					'Respuesta'  => utf8_encode($row['Respuesta']),
					'MarcaTiempo'  => str_replace(" ", "T", $row['MarcaTiempo']));
				$idx++;
         	}
         	return $ArrayOfNivelSatisfaccionUsuario;
		   } else
		   {
		   	return 'HTTP 404 "Not Found"';
		   }
	}
	 
	function ObtenerResultadosMensuales($IdInterventor,$MarcaTiempo)
	{
		$MarcaTiempo = str_replace("T", " ", $MarcaTiempo);
	      $sql = "SELECT * FROM WS_Indicadores WHERE IdInterventor = '$IdInterventor' AND MarcaTiempo >= '$MarcaTiempo'";
	      $link = Conectar();
	      $result = $link->query($sql);
			

		   if ( $result->num_rows > 0)
		   {
         	$idx = 0;
		   	while ($row = mysqli_fetch_assoc($result))
         	{
				$ArrayOfResultadoMensualIC[] = array(
					'IdInterventor'  => $row['IdInterventor'],
					'NumeroContrato'  => $row['NumeroContrato'],
					'Ano'  => $row['Ano'],
					'DaneDepartamento'  => $row['DaneDepartamento'],
					'DaneMunicipio'  => $row['DaneMunicipio'],
					'DaneInstitucion'  => $row['DaneInstitucion'],
					'DaneSede'  => $row['DaneSede'],
					'CodigoSimona'  => $row['CodigoSimona'],
					'IdBenefeciario'  => $row['IdBenefeciario'],
					'IdIndicador'  => $row['IdIndicador'],
					'CalculoIndicador'  => number_format($row['CalculoIndicador'], 4, ".", ""),
					'FechaCorte'  => $row['FechaCorte'],
					'MarcaTiempo'  => str_replace(" ", "T", $row['MarcaTiempo']));
				$idx++;
         	}
         	return $ArrayOfResultadoMensualIC;
		   } else
		   {
		   	return 'HTTP 404 "Not Found"';
		   }
	}

	function ObtenerAspectosFinancieros($IdInterventor,$MarcaTiempo)
	{
		$MarcaTiempo = str_replace("T", " ", $MarcaTiempo);
	      $sql = "SELECT * FROM WS_Financieros WHERE IdInterventor = '$IdInterventor' AND MarcaTiempo >= '$MarcaTiempo'";
	      $link = Conectar();
	      $result = $link->query($sql);
			

		   if ( $result->num_rows > 0)
		   {
         	$idx = 0;
		   	while ($row = mysqli_fetch_assoc($result))
         	{
				$ArrayOfAspectosFinancieros[] = array(
					'IdInterventor'  => $row['IdInterventor' ],
					'NumeroContrato'  => $row['NumeroContrato' ],
					'Ano'  => $row['Ano' ],
					'ValorContratoOperador'  => $row['ValorContratoOperador' ],
					'FechaFirmaContrato'  => $row['FechaFirmaContrato' ],
					'ValorAdicion'  => $row['ValorAdicion' ],
					'FechaProrrogaAdicion'  => $row['FechaProrrogaAdicion' ],
					'ValorDesembolso'  => $row['ValorDesembolso' ],
					'FechaPagoDesembolso'  => $row['FechaPagoDesembolso' ],
					'ValorAnticipo'  => $row['ValorAnticipo' ],
					'FechaAnticipo'  => $row['FechaAnticipo' ],
					'ValorUtilizacion'  => $row['ValorUtilizacion' ],
					'NumeroActaAprobacion'  => $row['NumeroActaAprobacion' ],
					'FechaUtilizacion'  => $row['FechaUtilizacion' ],
					'ValorRendimiento'  => $row['ValorRendimiento' ],
					'FechaRendimiento'  => $row['FechaRendimiento' ],
					'NumeroComprobanteRendimiento'  => $row['NumeroComprobanteRendimiento' ],
					'ValorComisionFiducia'  => $row['ValorComisionFiducia' ],
					'FechaComision'  => $row['FechaComision' ],
					'ValorGastosAdministrativos'  => $row['ValorGastosAdministrativos' ],
					'FechaGastosAdmisnitrativos'  => $row['FechaGastosAdmisnitrativos' ],
					'NombreFiducia'  => $row['NombreFiducia' ],
					'NumeroContratoFiducia'  => $row['NumeroContratoFiducia' ],
					'FechaContratoFiducia'  => $row['FechaContratoFiducia' ],
					'MarcaTiempo'  => str_replace(" ", "T", $row['MarcaTiempo']));
				$idx++;
         	}
         	return $ArrayOfAspectosFinancieros;
		   } else
		   {
		   	return 'HTTP 404 "Not Found"';
		   }
	}
	 
	$server->AddFunction("ObtenerAspectosFinancieros");
	$server->AddFunction("ObtenerNivelSatisfaccionUsuario");
	$server->AddFunction("ObtenerResultadosMensuales");
	$server->handle();
?>