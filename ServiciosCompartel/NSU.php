<?php

	include("../php/conectar.php"); 
   $link = Conectar();

   $encuestapreguntas = array(
      
  $encuestaPreguntas['0_1'] ='0.1 Fecha de Visita';,
  $encuestaPreguntas['0_2'] ='0.2 Hora de Visita';,
  $encuestaPreguntas['0_3'] ='0.3 Departamento/Municipio';,
  $encuestaPreguntas['0_4'] ='0.4 CODIGOS DE RESULTADO';,
  $encuestaPreguntas['0_5'] ='0.5 Entrevistador';,
  $encuestaPreguntas['0_6'] ='0.6 Supervisor';,
  $encuestaPreguntas['1_1'] ='1.1 Contratista';,
  $encuestaPreguntas['1_2'] ='1.2 No. Contrato';,
  $encuestaPreguntas['2_1'] ='2.1 Nombre del Beneficiario';,
  $encuestaPreguntas['2_10'] ='2.10 Estrato';,
  $encuestaPreguntas['2_11'] ='2.11 Correo Electronico';,
  $encuestaPreguntas['2_12'] ='2.12 Genero';,
  $encuestaPreguntas['2_2'] ='2.2 Documento de Identidad';,
  $encuestaPreguntas['2_3'] ='2.3 Id. Beneficiario';,
  $encuestaPreguntas['2_4'] ='2.4 Dirección';,
  $encuestaPreguntas['2_5'] ='2.5 Barrio';,
  $encuestaPreguntas['2_6'] ='2.6 Telefono';,
  $encuestaPreguntas['2_7'] ='2.7 Celular';,
  $encuestaPreguntas['2_8'] ='2.8 Municipio';,
  $encuestaPreguntas['2_9'] ='2.9 Departamento';,
  $encuestaPreguntas['3_1'] ='3.1 Nombre de quien atiende';,
  $encuestaPreguntas['3_2'] ='3.2 PARENTESCO';,
  $encuestaPreguntas['3_3'] ='3.3 CORREO ELECTRONICO';,
  $encuestaPreguntas['3_4'] ='3.4 GENERO ';,
  $encuestaPreguntas['3_6'] ='3.6 EDAD';,
  $encuestaPreguntas['3_7'] ='3.7 NIVEL EDUCATIVO ALCANZADO';,
  $encuestaPreguntas['3_8'] ='3.8 ES USTED CABEZA DE HOGAR?';,
  $encuestaPreguntas['3_9'] ='3.9 Cuál es su Ocupación Actual?';,
  $encuestaPreguntas['4_1'] ='4.1 Conoce Usted el Programa Hogares Digitales?';,
  $encuestaPreguntas['4_10'] ='4.10 Por que prefiere su uso?:';,
  $encuestaPreguntas['4_11A'] ='4.11a. Frecuencia de Chat/VideoChat/Redes Sociales';,
  $encuestaPreguntas['4_11B'] ='4.11b. Frecuencia de Transacciones/Pagos';,
  $encuestaPreguntas['4_11C'] ='4.11c. Frecuencia de Por actividades Laborales/Comerciales';,
  $encuestaPreguntas['4_11D'] ='4.11d. Frecuencia de Entretenimiento';,
  $encuestaPreguntas['4_11E'] ='4.11e. Frecuencia de Investigacion/Tareas/ Estudio/Noticias';,
  $encuestaPreguntas['4_11F'] ='4.11f. Frecuencia de Otros';,
  $encuestaPreguntas['4_2'] ='4.2 Cree usted que el acceso a Internet por medio del Programa HD le ha permitido:';,
  $encuestaPreguntas['4_3'] ='4.3 Considera que actualmente el Programa de H Digitales es:';,
  $encuestaPreguntas['4_4'] ='4.4 Con el acceso a Internet a traves del Programa Hogares digitales, considera que las relaciones con amigos y familiares ha: ';,
  $encuestaPreguntas['4_5'] ='4.5 El servicio de Internet del Programa Hogares Digitales, le ha permitido la oportunidades de realizar actividades comerciales, laborales u otras activides economicas? ';,
  $encuestaPreguntas['4_6'] ='4.6 Con qué frecuencia (en dias) usted usa el servicio de internet?';,
  $encuestaPreguntas['4_7'] ='4.7 Cuanto tiempo permanece conectado al servicio de internet?';,
  $encuestaPreguntas['4_8'] ='4.8 el horario habitual de uso del servicio de internet es?';,
  $encuestaPreguntas['4_9'] ='4.9 Utiliza Cabinas para uso del Internet? ';,
  $encuestaPreguntas['5_1'] ='5.1 Sabe donde reportar las fallas del servicio de INTERNET BANDA ANCHA?';,
  $encuestaPreguntas['5_2'] ='5.2 Cuando realiza el reporte de fallas del servicio de INTERNET BANDA ANCHA; estas han sido atendidas?';,
  $encuestaPreguntas['5_3'] ='5.3 El servicio de INTERNET ha presentado fallas en el ultimo mes?';,
  $encuestaPreguntas['5_4'] ='5.4 Usted reporto fallas presentadas en el ultimo mes?';,
  $encuestaPreguntas['5_5'] ='5.5 CUANTAS FALLAS REPORTO EN EL ULTIMO MES?';,
  $encuestaPreguntas['5_6'] ='5.6 CUANTO TIEMPO TRANSCURRIO PARA SER ATENDIDA LA FALLA?';,
  $encuestaPreguntas['5_7'] ='5.7 CUAL FUE EL TIPO DE FALLA QUE PRESENTO EL SERVICIO?';,
  $encuestaPreguntas['5_8'] ='5.8 RECIBE SU FACTURA A TIEMPO (PUNTUALIDAD)?';,
  $encuestaPreguntas['5_9A'] ='5.9a. Percepción de Atención recibida a reclamaciones que ha realizado por el servicio';,
  $encuestaPreguntas['5_9B'] ='5.9b. Percepción de Atención que ha recibido del servicio a través de las lineas telefonicas o el Centro de Atención al Usuario';,
  $encuestaPreguntas['5_9C'] ='5.9c. Percepción de Disposicion de las personas que atienden las solicitudes de servicio';,
  $encuestaPreguntas['6_1'] ='6.1 Autoriza toma de Fotografía de la fachada del predio, donde se observe claramente la dirección de vivienda?';
);

   $sql = "SELECT 
            encuestas.Prefijo,
            encuestas.fechaInicio,
            encuestas.fechaFin,
            CodDane_Departamentos.Departamento,
            CodDane_Municipios.NomMunicipio,
            CONCAT(Beneficiarios.Departamento, Beneficiarios.Municipio) AS 'CodDane',
            Beneficiarios.Departamento AS 'CodDepartamento',
            Beneficiarios.Municipio AS 'CodMunicipio',
            DatosUsuarios.Nombre AS 'Usuario',
            Beneficiarios.Contrato,
            Beneficiarios.Nombre AS 'BeneficiarioNombre',
            Beneficiarios.Documento AS 'BeneficiarioDocumento',
            encuestas.idBeneficario AS 'BeneficiarioId',
            Beneficiarios.Direccion AS 'BeneficiarioDireccion',
            Beneficiarios.Barrio AS 'BeneficiarioBarrio',
            Beneficiarios.Telefono AS 'BeneficiarioTelefono',
            Beneficiarios.Celular AS 'BeneficiarioCelular',
            Beneficiarios.Estrato AS 'BeneficiarioEstrato',
            Beneficiarios.Correo AS 'BeneficiarioCorreo',
            Beneficiarios.Genero AS 'BeneficiarioGenero',
            encuestas.Resultado
         FROM 
            encuestas 
            INNER JOIN DatosUsuarios ON DatosUsuarios.idLogin = encuestas.idLogin
            INNER JOIN Beneficiarios ON Beneficiarios.idBeneficiario = encuestas.idBeneficario
            INNER JOIN CodDane_Departamentos ON CodDane_Departamentos.Codigo = Beneficiarios.Departamento
            INNER JOIN CodDane_Municipios ON CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento AND CodDane_Municipios.CodMunicipio =  Beneficiarios.Municipio;";
   
   $result = $link->query($sql);

   if ($result->num_rows > 0)
   {
            
      $idx = 0;
      $idy = 0;
      $idBeneficiario = 0;

      $Resultados = array();
         while ($row = mysqli_fetch_assoc($result))
         { 
            $idBeneficiario = $row['BeneficiarioId'];
            $Resultados[$row['BeneficiarioId']] = array();
            $Resultados[$row['BeneficiarioId']]['Prefijo'] = $row['Prefijo'];
            $Resultados[$row['BeneficiarioId']]['idBeneficiario'] = $row['BeneficiarioId'];
            $Resultados[$row['BeneficiarioId']]['fechaInicio'] = $row['fechaInicio'];
            $Resultados[$row['BeneficiarioId']]['fechaFin'] = $row['fechaFin'];
            $Resultados[$row['BeneficiarioId']]['Departamento'] = $row['Departamento'];
            $Resultados[$row['BeneficiarioId']]['NomMunicipio'] = $row['NomMunicipio'];
            $Resultados[$row['BeneficiarioId']]['CodDane'] = $row['CodDane'];
            $Resultados[$row['BeneficiarioId']]['Usuario'] = $row['Usuario'];
            $Resultados[$row['BeneficiarioId']]['BeneficiarioNombre'] = trim($row['BeneficiarioNombre']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioDocumento'] = trim($row['BeneficiarioDocumento']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioDireccion'] = trim($row['BeneficiarioDireccion']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioBarrio'] = trim($row['BeneficiarioBarrio']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioTelefono'] = trim($row['BeneficiarioTelefono']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioCelular'] = trim($row['BeneficiarioCelular']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioEstrato'] = trim($row['BeneficiarioEstrato']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioCorreo'] = trim($row['BeneficiarioCorreo']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioGenero'] = trim($row['BeneficiarioGenero']);
            
            $tmpResultado = explode("##", $row['Resultado']);

            $preFila = "<tr>
                           <td>IntW11</td>
                           <td>520</td>
                           <td>2015</td>
                           <td>" . $row['CodDepartamento'] . "</td>
                           <td>" . $row['CodMunicipio'] . "</td>
                           <td>NO APLICA</td>
                           <td>NO APLICA</td>
                           <td>00000</td>
                           <td>" . $row['BeneficiarioId'] ."</td>
                           <td>WSP1711</td>
                           <td>V1</td>
                           <td>XX</td>
                           <td>" . $row['fechaInicio'] ."</td>";

            foreach ($tmpResultado as $key => $value) 
            {
               if ($value <> "")
               {
                  $tmpValor = explode('->', $value);
                  if ($tmpValor[0] == "5_4")
                  {
                     $tmpValor[0] = "5_" . (4 + $idy);
                     $idy++;
                  } else
                  {
                     $idy = 0;
                  }
                  $Resultados[$row['BeneficiarioId']][$tmpValor[0]] = $tmpValor[1];
               }
            }

            $idx++;
         }

         /****************** Obtener Archivo de Resultados ******/
            $Encabezado = "<tr>";
            $Salida = "";
            
                  $Encabezado .= "<th>IdInterventor</th>";
                  $Encabezado .= "<th>NumeroContrato</th>";
                  $Encabezado .= "<th>Ano</th>";
                  $Encabezado .= "<th>DaneDepartamento</th>";
                  $Encabezado .= "<th>DaneMunicipio</th>";
                  $Encabezado .= "<th>DaneInstitucion</th>";
                  $Encabezado .= "<th>DaneSede</th>";
                  $Encabezado .= "<th>CodigoSimona</th>";
                  $Encabezado .= "<th>NumeroReferenciaPago</th>";
                  $Encabezado .= "<th>CodigoTipoFormulario</th>";
                  $Encabezado .= "<th>VersionTipoFormulario</th>";
                  $Encabezado .= "<th>IdFormulario</th>";
                  $Encabezado .= "<th>FechaEncuesta</th>";
                  $Encabezado .= "<th>Pregunta</th>";
                  $Encabezado .= "<th>Respuesta</th>";
                  $Encabezado .= "<th>MarcaTiempo</th>";
            $Encabezado .= "</tr>";

            foreach ($Resultados as $key => $val) 
            {
               $Salida .= "<tr>";
               foreach ($val as $key => $value) 
               {
                  if ($key == "Prefijo")
                  {
                     $Salida .= "<td>'" . $value . "</td>";
                  } else
                  {
                     $Salida .= "<td>" . $value . "</td>";  
                  }
               }
               $Salida .= "</tr>";
            }
               mysqli_free_result($result);
            
            class Excel
            {
               public $Encabezado;
               public $Cuerpo;
            }

            $Respuesta = new Excel();
            $Respuesta->Encabezado = $Encabezado;
            $Respuesta->Cuerpo = $Salida;

            echo $Encabezado;
            //echo $Salida;

         /*********************************************************/
         
   } else
   {
      echo 0;
   }
?>