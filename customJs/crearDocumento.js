function crearDocumento()
{
  $("#txtCrearArchivo_FechaEnvio, #txtCrearArchivo_FechaRadicado").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });
  
  $("#btnCrearDocumento_BuscarSinAsociar").on("click", btnCrearDocumento_BuscarSinAsociar_Click);
  $(".trCrearDocumento_Documento").live("dblclick", trCrearDocumento_Documento_dblClick);

  $("#btnCrearDocumento_ReindexarArchivos").on("click", btnCrearDocumento_ReindexarArchivos_Click);

  cargarArchivosSinAsociar();
}
function cargarArchivosSinAsociar()
{
  $("#tblCrearDocumento_SinAsociar tbody tr").remove();
  $.post('php/cargarArchivosSinAsociar.php', {}, 
    function(data, textStatus, xhr) 
    {
      if (data != 0)
      {
        $("#msgCrearDocumento_NoHayArchivos").hide();
        $("#tblCrearDocumento_SinAsociar").show();
        var icono = "";
        var extension = "";

        var tds = '';

        $.each(data, function(index, val) 
        { 
           switch (val.Extension)
              {
                case ".xls":
                  icono = "xls";
                  break;
                case "xlsx":
                  icono = "xls";
                  break;
                case ".doc":
                  icono = "doc";
                  break;
                case "docx":
                  icono = "doc";
                  break;
                case ".gif":
                  icono = "jpg";
                  break;
                case ".jpg":
                  icono = "jpg";
                  break;
                case ".jpeg":
                  icono = "jpg";
                  break;
                case ".png":
                  icono = "jpg";
                  break;
                case ".pdf":
                  icono = "pdf";
                  break;
                case ".ppt":
                  icono = "ppt";
                  break;
                case "pptx":
                  icono = "ppt";
                  break;
                case ".zip":
                  icono = "zip";
                  break;
                case ".rar":
                  icono = "zip";
                  break;
                default:
                  icono = "eps";
              }

              tds += '<tr class="trCrearDocumento_Documento" idArchivo="' + val.Nombre + '">';
              tds += '   <td>';
              tds += '       <img src="img/file-search/' + icono + '.png" alt="">';
              tds += '       <strong><a  id="lblArchivo_'+ val.idArchivo + '" target="_blank" href="Archivos/files/' + val.Ruta + '/' + val.Nombre + '">' + val.Nombre + '</a></strong>';
              tds += '       ' + val.Ruta;
              tds += '   </td>';
              tds += '   <td>' + notacionTamanio(val.Peso) + '</td>';
              tds += '   <td>' + val.Fecha + '</td>';
              tds += ' </tr>';
        });    
        $("#tblCrearDocumento_SinAsociar tbody").append(tds);
      } else
      {
        $("#msgCrearDocumento_NoHayArchivos").show();
        $("#tblCrearDocumento_SinAsociar").hide();
      }
    }, "json").fail(function()
    {
      Mensaje("Error", "No hay conexión con el servidor.");
    });
}
function btnCrearDocumento_BuscarSinAsociar_Click(evento)
{
  evento.preventDefault();
  $.post('php/Archivos.php', {}, function(data, textStatus, xhr) 
  {
    cargarArchivosSinAsociar();
  });
  
}
function notacionTamanio(peso)
{
  var Unidad = "";
  if (peso != "")
  {
    Unidad = peso + " B";
    if (peso > 1048576)
    {
      Unidad = ((peso/1024)/1024).toFixed(2) + " MB";
    } else
    {
      if (peso > 1024)
      {
        Unidad = (peso/1024).toFixed(2) + " KB";
      } 
    }
  } else
  {
    Unidad = "0 B";
  }
  return Unidad;
}

function trCrearDocumento_Documento_dblClick() 
  {
    var objFila = $(this);
    var titulo = "Asociar: " + $("#lblArchivo_" + $(objFila).attr("idArchivo")).text();

    $("#frmAsociarDocumento input").val("");
    $("#frmAsociarDocumento").attr("idArchivo", $(objFila).attr("idArchivo"));
    $("#frmAsociarDocumento").dialog(
      {
        buttons: [
        {
          text: "Guardar",
          icons: {
            primary: "ui-icon-disk"
          },
          click: function() 
          {
            if (validarFormulario($("#frmAsociarDocumento")))
            {
             $.post("php/asociarDocumento.php", 
              {
                idLogin : Usuario.id, 
                idArchivo : $(objFila).attr("idArchivo"), 
                Tipo : $('#txtCrearArchivo_Tipo').val(),
                Operador : $('#txtCrearArchivo_Operador').val(),
                FechaEnvio : $('#txtCrearArchivo_FechaEnvio').val(),
                FechaRadicado : $('#txtCrearArchivo_FechaRadicado').val(),
                Consecutivo : $('#txtCrearArchivo_Consecutivo').val(),
                RadicadoMinTIC : $('#txtCrearArchivo_RadicadoMinTIC').val(),
                RadicadoOperador : $('#txtCrearArchivo_RadicadoOperador').val(),
                RadicadoWSP : $('#txtCrearArchivo_RadicadoWSP').val(),
                Asunto : $('#txtCrearArchivo_Asunto').val(),
                PalabrasClave : $('#txtCrearArchivo_PalabrasClave').val()
              }, 
              function(data)
              {
                if (data.length < 3)
                {
                  $(objFila).remove();
                  Mensaje("Ok", "El documento ha quedado asociado");
                } else
                {
                  Mensaje("Error", data);
                }


              }).fail(function() {
                      Mensaje("Error", "No fue posible enviar los datos");
                    });;
              
              $( this ).dialog( "close" );
            }
          }
        }],
          title: titulo,
          width: 500
      });


    /*
    var tds = "";
    tds += '<div class="alert alert-success" id="msgCrearDocumento_NoHayArchivos">';
    tds += '<button class="close" data-dismiss="alert">×</button>';
    tds += '<strong>!</strong> Todos los archivos estan Asociados.';
    tds += '</div>';
    $(this).append(tds);*/

  }
  function btnCrearDocumento_ReindexarArchivos_Click(evento)
  {
    evento.preventDefault();
    $.post("php/Archivos.php", {}, function()  
    {  
      Mensaje(";)", "Tolis");
    });
  }