function reporteEncuestas()
{
  $("#btnReporteEncuestas_BusquedaAvanzada").on("click", btnReporteEncuestas_BusquedaAvanzada_Click);
  $("#btnReporteEncuestas_DescargarMatriz").on("click", btnReporteEncuestas_DescargarMatriz_Click);
  $("#frmReporteEncuestas").on("submit", frmReporteEncuestas_Submit);
}
function btnReporteEncuestas_BusquedaAvanzada_Click(evento)
{
  evento.preventDefault();
  var obj = $("#frmReporteEncuestas_BusquedaAvanzada").is(":visible");
  if (obj)
  {
    $("#frmReporteEncuestas_BusquedaAvanzada").slideUp();
  } else
  {
    $("#frmReporteEncuestas_BusquedaAvanzada").slideDown();
  }
}
function btnReporteEncuestas_DescargarMatriz_Click(evento)
{
  evento.preventDefault();
  var datos = {
    "fechaIni" : $("#txtReporteEncuestas_Desde").val(),
    "fechaFin" : $("#txtReporteEncuestas_Hasta").val(),
    "CodBeneficiario" : $("#txtReporteEncuestas_CodBeneficiario").val(),
    "Departamento" : $("#txtReporteEncuestas_Departamento").val(),
    "Municipio" : $("#txtReporteEncuestas_Municipio").val(), 
    "Direccion" : $("#txtReporteEncuestas_Direccion").val(), 
    "Tipo" : $("#txtReporteEncuestas_Tipo").val() 
  };
  $.post("php/cargarEncuestasDetalle.php", {datos : datos}, function(data)
    {
      if (data != 0)
      {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "php/excel.php");

        form.setAttribute("target", "view");

        var txtCuerpo = document.createElement("input"); 
        txtCuerpo.setAttribute("type", "hidden");
        txtCuerpo.setAttribute("name", "Cuerpo");
        txtCuerpo.setAttribute("value", data.Cuerpo);
        
        var txtEncabezado = document.createElement("input"); 
        txtEncabezado.setAttribute("type", "hidden");
        txtEncabezado.setAttribute("name", "Encabezado");
        txtEncabezado.setAttribute("value", data.Encabezado);

        var txtNombre = document.createElement("input"); 
        txtNombre.setAttribute("type", "hidden");
        txtNombre.setAttribute("name", "Nombre");
        txtNombre.setAttribute("value", "Reporte_" + $("#txtReporteEncuestas_Desde").val() + "_" + $("#txtReporteEncuestas_Hasta").val());
        
        form.appendChild(txtCuerpo);
        form.appendChild(txtEncabezado);
        form.appendChild(txtNombre);
        document.body.appendChild(form);

        window.open('', 'view');
        form.submit();
        $(form).remove();
      } else
      {
        alert("No hay datos para descargar");
      }
    }, "json");

}
function frmReporteEncuestas_Submit(evento)
{
  evento.preventDefault();
  var datos = {
    "fechaIni" : $("#txtReporteEncuestas_Desde").val(),
    "fechaFin" : $("#txtReporteEncuestas_Hasta").val(),
    "CodBeneficiario" : $("#txtReporteEncuestas_CodBeneficiario").val(),
    "Departamento" : $("#txtReporteEncuestas_Departamento").val(),
    "Municipio" : $("#txtReporteEncuestas_Municipio").val(), 
    "Direccion" : $("#txtReporteEncuestas_Direccion").val(), 
    "Tipo" : $("#txtReporteEncuestas_Tipo").val() 
  };
  $.post("php/cargarEncuestas.php", {datos : datos}, function(data)
    {
      if (data == 0)
      {
        Mensaje("Vacío", "No hay datos para los parámetros de búsqueda");
      } else
      {
        $("#tblEncuestas").dataTable().fnDestroy();
        $("#tblEncuestas tbody tr").remove();
        var tds = "";
        $.each(data, function(index, value)
          {
            tds += '<tr class="odd gradeX">';
            tds += "<td></td>";
            tds += "<td>" + value.idBeneficiario +"</td>";
            tds += "<td>" + value.Fecha +"</td>";
            tds += "<td>" + value.Hora +"</td>";
            tds += "<td>" + value.Departamento +"</td>";
            tds += "<td>" + value.NomMunicipio +"</td>";
            tds += "<td>" + value.Direccion +"</td>";

            if (value.Tipo == "Visita")
            {
              tds += "<td><i class='icon-plane'></i> Visita</a></td>";  
            } else
            {
              tds += "<td><i class='icon-phone'></i> Llamada</a></td>";  
            }

            if (value.Coordenadas != "" && value.Coordenadas != "Telefonica")
            {
              tds += "<td><a target='_blank' href='https://www.google.com/maps/place/" + value.Coordenadas + "'><i class='icon-map-marker'></i> Ubicación</a></td>";  
            } else
            {
              tds += "<td></td>";
            }

            tds += "<td><a target='_blank' href='php/generarEncuesta.php?id=" + value.idBeneficiario + "'><i class='icon-list-alt'></i> Detalles</a></td>";  
            tds += "</tr>";
            
          });
        $("#tblEncuestas tbody").append(tds);
        $("#tblEncuestas").crearTabla1({lblMenu : "Datos por página"});

      }
    }, "json");

}