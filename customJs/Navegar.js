function NavegadorArchivos()
{
  //$.post("php/Archivos.php", {}, function()
  //{
    iniciarFinder("hd", '', $("#arbolInicial"));
  //});

  $("#txtBuscarArchivo_FechaEnvio, #txtBuscarArchivo_FechaRadicado").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

  $("#btnBuscarArchivo_CerrarBusqueda").on("click", function(evento)
    {
      evento.preventDefault();
      cerrarBusqueda();
    });
  $("#btnBuscarArchivo_BusquedaAvanzada").on("click", BusquedaAvanzada);

  $("#frmBuscarArchivo").on("submit", frmBuscarArchivo_submit);
  $("#frmBuscarArchivo_BusquedaAvanzada").on("submit", frmBuscarArchivo_BusquedaAvanzada_submit);
  

  $(".lblBuscarArchivo_AbrirCarpeta").live("click", BuscarArchivo_AbrirCarpeta);
  $(".lblBuscarArchivo_AbrirArchivo").live("click", BuscarArchivo_AbrirArchivo);
}
function frmBuscarArchivo_submit(evento)
{
  evento.preventDefault();

  $.post("php/cargarDocumentos.php", {Parametro : $("#txtBuscarArchivo_Parametro").val()}, 
    function(data)
    {
      listarResultados(data); 
    }, "json").fail(function() {
            Mensaje("Error", "No fue posible cargar los Documentos");
          });;
}
function BuscarArchivo_AbrirArchivo()
{
  window.open("Archivos/files/" + $(this).attr("Ruta"));
}

function BuscarArchivo_AbrirCarpeta()
{
  cerrarBusqueda();
  $('#arbolInicial').elfinder('destroy');
  iniciarFinder("hd", $(this).text(), $("#arbolInicial"));

}
function cerrarBusqueda()
{
  $("#BuscarArchivo_Resultados div").remove();
  $("#lblBuscarArchivo_Help").text("");
}
function listarResultados(data)
{
  cerrarBusqueda();
  if (data != 0)
  {
    $("#lblBuscarArchivo_Help").text(data.length + " resultados encontrados");
    var tds = "";

    $.each(data, function(index, value) 
      {
        tds += '<div class="classic-search">';
        tds +=      '<h4><a href="#" class="lblBuscarArchivo_AbrirArchivo" Ruta="' + value.Ruta + '/' + value.Nombre + '">' + value.Tipo + ' ' + value.Operador + ' ' + value.Consecutivo + '</a></h4>';
        tds +=      '<a href="#" class="lblBuscarArchivo_AbrirCarpeta">' + value.Ruta + '</a>';
        tds +=      '<p>'+ value.Asunto +'</p>';
        tds +=  '</div>';
      });

    $("#BuscarArchivo_Resultados").append(tds);
  } else
  {
    $("#lblBuscarArchivo_Help").text("No se encontraron resultados");
  }
}
function BusquedaAvanzada(evento)
{
  evento.preventDefault();

  if ($("#frmBuscarArchivo_BusquedaAvanzada").is(':visible'))
  {
    $("#frmBuscarArchivo_BusquedaAvanzada").slideUp();
  } else
  {
    $("#frmBuscarArchivo_BusquedaAvanzada").slideDown();
  }
}
function frmBuscarArchivo_BusquedaAvanzada_submit(evento)
{
  evento.preventDefault();

  $.post("php/BusquedaAvanzada.php", 
    {
      Tipo : $('#txtBuscarArchivo_Tipo').val(),
      Operador : $('#txtBuscarArchivo_Operador').val(),
      FechaEnvio : $('#txtBuscarArchivo_FechaEnvio').val(),
      FechaRadicado : $('#txtBuscarArchivo_FechaRadicado').val(),
      Consecutivo : $('#txtBuscarArchivo_Consecutivo').val(),
      RadicadoMinTIC : $('#txtBuscarArchivo_RadicadoMinTIC').val(),
      RadicadoOperador : $('#txtBuscarArchivo_RadicadoOperador').val(),
      RadicadoWSP : $('#txtBuscarArchivo_RadicadoWSP').val(),
      Asunto : $('#txtBuscarArchivo_Asunto').val(),
      PalabrasClave : $('#txtBuscarArchivo_PalabrasClave').val()
    }, 
    function(data)
    {
      listarResultados(data); 
    }, "json").fail(function() {
            Mensaje("Error", "No fue posible cargar los Documentos");
          });;
}