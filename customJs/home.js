var Usuario = null;

$(document).on("ready", appReady);

function appReady()
{
           
  Usuario = JSON.parse(localStorage.getItem('MinTIC_HD'));  
  cagarBarraSuperior(Usuario);

  expirarSesion();
  
  $("body").on('click', expirarSesion);

  //$(this).cargarModulo({pagina : "Navegar", titulo : "Navegar", icono : "icon-dashboard"}, cargarInicio);

  //setInterval(hacerPush, 1800000);
}

function cagarBarraSuperior(pUsuario)
{

  $("#header").load("header.html", function()
    {
      $(".username").text(pUsuario.nombre);
      $("#lblHeaderUsuario a").attr("idLogin", pUsuario.id);
      $("#FotoPerfil").attr("src", pUsuario.Foto);
      $("#FotoPerfil").css("width", "35px");
      $("#FotoPerfil").css("height", "35px");
    });
  
     
    $("#sideMenu").load("menu.html", function()
    {
      $("#sideMenu li").on("click", sideMenu);
      App.init();
    });
  

}

$.fn.cargarCorreos = function(options, callback)
      {
        var defaults =
        {
          idUsuario : "0",
          Estado: 'Pendiente',
          Tipo: ''
        }
        var options = $.extend(defaults, options);
        if (callback === undefined)
        {callback = function(){};}

      /*plugin*/
      
        $.post("php/cargarCorreos.php", 
          {pUsuario : options.idUsuario, pEstado: options.Estado, pTipo : options.Tipo},
          function(data, textStatus, xhr)
          {
            callback(data);
          },"json");
      /*plugin*/

      //Averigua si el parámetro contiene una función de ser así llamarla
        if($.isFunction(options.onComplete)) 
        {options.onComplete.call();}
      };
function sideMenu(event)
{
  if (!($(this).hasClass('has-sub')))
  {
    if ($(this).text() == "Navegar")
    {
      $("#contenedorModulos").hide();
      $("#contenedorNavegar").show();
    } else
    {
      $("#contenedorModulos").show();
      $("#contenedorNavegar").hide();
      $(this).cargarModulo({pagina : $(this).attr("pagina"), titulo : $(this).text(), icono : $(this).attr("icono")});
    }
    
    
  }  
}
function hacerPush (argument) 
{
  comprobarActualizacion();
  ObtenerCoordenadas("txtCoordenadas", false);
  $("#desplegableCorreos").cargarCorreos({idUsuario: Usuario.id, Estado: 'Pendiente', Tipo : 'Recibidos'}, 
    function(Correos)
    {
      $("#desplegableCorreos li").remove();
      $(".lblContadorMensajes").hide();
      var s_ = "";

      var tds = "<li><p>Tienes <span class='lblContadorMensajes'>0</span> nuevo"+ s_ + " mensaje" + s_ +"</p></li>";
      $.each(Correos, function(index, value)
        {
          if (Correos.length > 1)
          { s_ = "s"; }

          if (index < 5)
          {
            tds += "<li><a href='#'>";
            //tds += '<span class="photo"><i class="icon-envelope"></i></span>';
            //tds += '<span class="photo"><img src="./img/avatar-mini.png" alt="avatar"></span>';
            
            tds += '<span class="subject">';
            tds += '  <span class="from">' + value.nombreRemitente + '</span><br />';

            tds += '  <span class="time">' + value.Fecha + '</span><br />';
            tds += '</span>';
            tds += '<span class="message">' + value.Asunto + '</span>';
            tds += '</a></li>';
          }
        });
      //tds += "<li><a href='inbox.html'>Ver todos los mensajes</a></li>";
      $("#desplegableCorreos").append(tds);
      if (Correos.length > 0)
      {
        $(".lblContadorMensajes").text(Correos.length);  
        $(".lblContadorMensajes").show();
      }
      
    });
}

$.fn.crearTabla1 = function(options, callback)
{
  var idTabla = $(this).attr("id");
  var defaults =
  {
    lblMenu : "Registros por página"
  }
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
  
  $('#' + idTabla).dataTable().fnDestroy();
//"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",  
  tabla = $('#' + idTabla).dataTable({
            "sDom": 'CTW<"clear">lfrtip',
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ " + options.lblMenu + "",
                "oPaginate": {
                    "sPrevious": "Ant",
                    "sNext": "Sig"
                }
            },
            "oTableTools": 
            {
          "sSwfPath": "assets/data-tables/media/swf/copy_csv_xls_pdf.swf",
          "aButtons": [
              {
                "sExtends": "copy",
                "sButtonText": "Copiar"
              },
              {
                "sExtends": "csv",
                "sButtonText": "CSV"
              },
              {
                "sExtends": "xls",
                "sButtonText": "Excel"
              }
            ]
            }
        });

        jQuery('#' + idTabla + '_wrapper .DTTT').addClass("span6 offset10"); // modify table search input
        jQuery('#' + idTabla + '_wrapper .dataTables_filter input').addClass("input-medium"); // modify table search input
        jQuery('#' + options.idTabla + '_wrapper .dataTables_length select').addClass("input-mini"); // modify table per page dropdown
/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};
$.fn.cargarModulo = function(options, callback)
{
  var defaults =
  {
    pagina : "404",
    titulo : "Funcion no Encontrada",
    icono : "icon-ban-circle"
  };
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
    var nonModulo = options.pagina;
  nomModulo = options.pagina;
  nomModulo = "modulo_" + nomModulo.replace(/\s/g, "_");
  nomModulo = nomModulo.replace(/\./g, "_");
  nomModulo = nomModulo.replace(/\//g, "_");
  var tds = "";
  $(".widget-body").slideUp();
  $(".icon-chevron-up").removeClass("icon-chevron-up").addClass("icon-chevron-down");

  if ($("#main_menu_trigger").is(":visible"))
  {
    $("#sideMenu").slideUp();
  } 

  if ($('#' + nomModulo).length)
  {
     $('#' + nomModulo).show('slide');
     $("#" + nomModulo + " .widget-body").slideDown();
     $("#" + nomModulo + " .icon-chevron-down").removeClass("icon-chevron-down").addClass("icon-chevron-up");
     $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
     callback();
  } else
  {
    tds += '<div id="' + nomModulo + '" class="">';
    tds += '  <div class="widget">';
    tds += '        <div class="widget-title">';
    tds += '           <h4><i class="' + options.icono + '"></i><span>Cargando...</span></h4>';
    tds += '           <span class="tools">';
    tds += '           <a href="javascript:;" class="icon-chevron-up btnMinimizar"></a>';
    tds += '           <a class="icon-remove btnCerrar" style="font-size:2em;" href="javascript:;"></a>';
    tds += '           </span>';
    tds += '        </div>';
    tds += '        <div class="widget-body">';
    tds += '        </div>';
    tds += '  </div>';
    tds += '</div>';
    
    $("#contenedorModulos").append(tds);  
     var n = options.pagina.search(".html");
     if (n < 1)
     {
        options.pagina = options.pagina + ".html";
     }
    $.post("cargarFrame.php", {archivo: options.pagina}, 
      function(data)
      {
        $("#" + nomModulo + " .widget-body").html(data);  
        
        //$("#" + nomModulo + " .widget-body").html("<h1>Listo</h1>");
      }, "html").always(function()
      {
          $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
          /*$("#" + nomModulo + " .widget-title h4 i").removeClass('*');
          $("#" + nomModulo + " .widget-title h4 i").addClass(icono);        
          */
          minimizarPorlet(nomModulo);
          cerrarPorlet(nomModulo);
          callback();
      });
  }

/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};

function minimizarPorlet(id)
{
  //jQuery('#' + id + ' .widget .tools .icon-chevron-down, #' + id + '.widget .tools .icon-chevron-up').on("click", function () {
  /*jQuery('#' + id + ' .btnMinimizar').on("click", function () {
            var el = $("#" + id + " .widget-body");
            var objTitulo = $("#" + id + " .btnMinimizar");
            if (jQuery(objTitulo).hasClass("icon-chevron-down")) {
                jQuery(objTitulo).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideDown(200);
            } else {
                jQuery(objTitulo).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideUp(200);
            }
        });*/
  $("#" + id + " .widget-title").on("click", function(event) 
  {
      var el = $("#" + id + " .widget-body");
            var objTitulo = $("#" + id + " .btnMinimizar");
            if (jQuery(objTitulo).hasClass("icon-chevron-down")) {
                jQuery(objTitulo).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideDown(200);
            } else {
                jQuery(objTitulo).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideUp(200);
            }
  });
}
function cerrarPorlet(id)
{
  jQuery('#' + id + ' .btnCerrar').on("click", function () 
    {
        jQuery('#' + id).hide('slide');
    });
}
function cargarInicio()
{
  
}

HTTP_GET_VARS=new Array();
strGET=document.location.search.substr(1,document.location.search.length);
if(strGET!='')
    {
    gArr=strGET.split('&');
    for(i=0;i<gArr.length;++i)
        {
        v='';vArr=gArr[i].split('=');
        if(vArr.length>1){v=vArr[1];}
        HTTP_GET_VARS[unescape(vArr[0])]=unescape(v);
        }
    }

function GET(v) {
  if(!HTTP_GET_VARS[v]){return 'undefined';}
  return HTTP_GET_VARS[v];
}

function cargarPerfilUsuario()
{
  var pIdLogin = $(this).attr("idLogin");
  var pTexto = $(this).text();;
 $(this).cargarModulo({pagina : "profile", titulo : "Perfil de " + pTexto, icono : "icon-user"}, function()
 {
    cargarProfile(pIdLogin);
 }); 
}
function cargarArchivos(titulo, idCarpeta)
{
  $(this).cargarModulo({pagina : "verArchivos", titulo : "Archivos de " + titulo, icono : "icon-copy"}, function()
      {
        if($('#verArchivos').elfinder('instance'))
        { $('#verArchivos').elfinder('destroy');}
       
        var elf = $('#verArchivos').elfinder({
              url : 'Archivos/php/connector.php?Contrato=' + idCarpeta,
              lang: 'es',  
              handlers:
              {
                upload : function(event) 
                    { 
                      
                    },
                open: function(event)
                    { 
                      
                    },
                rm : function(event)
                    {
                      
                    }
              }           
        }).elfinder('instance');
      });
}
function cargarArchivos_Usuario_Click()
{
  cargarArchivos($(this).attr("Nombre"), "Usuarios\/" + $(this).attr("idLogin"));
}
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function fechasRango(desde, hasta)
{
  $( desde ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( hasta ).datepicker( "option", "minDate", selectedDate );
          }
        });
        $( hasta ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( desde ).datepicker( "option", "maxDate", selectedDate );
          }
        });
}
function checkValor(idObjeto)
{
  return $("#"+ idObjeto).is(":checked");
}
function ObtenerCoordenadas(idControl, pMensaje)
{
  var objCoordenadas ="";
  $("#" + idControl).val(objCoordenadas); 
  navigator.geolocation.getCurrentPosition(
    function(datos)
    {
      var lat = datos.coords.latitude;
      var lon = datos.coords.longitude;
      var accu = datos.coords.accuracy;

      objCoordenadas =  lat + "," + lon + "#" + accu;
        if (pMensaje != false)
        {
            Mensaje("Ubicación", objCoordenadas);
        }
      $("#" + idControl).val(objCoordenadas);
    }, 
    function ()
    {
      objCoordenadas ="No hay precision en el dato";
      if (pMensaje != false)
      {
        Mensaje("Ubicación", objCoordenadas);
      }
    });
  return objCoordenadas;
}
function comprobarActualizacion()
{
  $.post("php/actualizacion.php", {version : 1, idLogin : Usuario.id}, function(data)
    {
      if (data != "")
      {
        Mensaje("Hey", data);
      }
    });
}

function cargarConvenciones(datos2, idContenedor, idGrafica)
{
  var tmpObj = $("#" + idGrafica + " .flotr-legend-color-box");
        $("#" + idContenedor + " article").remove();
        $.each(tmpObj, function(index, value)
        {
          var objHtml = $(value).html();
                  
          var strObj = objHtml.substring(parseInt(objHtml.indexOf("(") + 1 ), parseInt(objHtml.indexOf(")")) );
          var strObj_ = strObj.split(",");
          $("#" + idContenedor + "").append("<article><div style='width:1em; height:1em;background:#"+ rgbToHex(strObj_[0], strObj_[1], strObj_[2]) + ";'></div><span> " + datos2[index].label+"</span></article>");

        });
        $(".flotr-legend").hide();
}
function cargarGraficaBarras(datos2, titulo, idContenedor)
{
  var Contenedor = document.getElementById(idContenedor);
  graph = Flotr.draw(Contenedor,
          datos2
          , {
            title: titulo,
              bars : {
                show : true,
                horizontal : false,
                shadowSize : 5,
                barWidth: 1
              },
              legend: 
              {
                backgroundOpacity: 0,
                position: 'ne'
              },
              grid : 
              {
                verticalLines : false,
                horizontalLines : true
              },
              mouse:
              {
                relative: true,
                track: true,
                tackAll: true,
                trackDecimals: 0,
                trackFormatter: function (x) 
                {
                    return x.series.label +": " + x.series.data[0][1];
                  }
              },
               xaxis: 
              {   autoscale: true,
                showLabels: false   },
              yaxis:
              {   
                autoscale:true,
                autoscaleMargin : 1,
                min: 0,
                showLabels: true,
                tickDecimals: 0
              }
            });
}


function cargarGraficaBarras2(datos2, titulo, idContenedor)
{
  var Contenedor = document.getElementById(idContenedor);
  graph = Flotr.draw(Contenedor,
          datos2
          , {
            title: titulo,
              bars : {
                show : true,
                horizontal : false,
                shadowSize : 5,
                barWidth: 1
              },
              legend: 
              {
                backgroundOpacity: 0,
                position: 'ne'
              },
              grid : 
              {
                verticalLines : false,
                horizontalLines : true
              },
              mouse:
              {
                relative: true,
                track: true,
                tackAll: true,
                trackDecimals: 0,
                trackFormatter: function (x) 
                {
                    return x.series.label +": " + x.series.data[0][1];
                  }
              },
               xaxis: 
              {   autoscale: true,
                showLabels: false   },
              yaxis:
              {   
                autoscale:true,
                autoscaleMargin : 1,
                min: 0,
                showLabels: true,
                tickDecimals: 0
              }
            });
}

function rgbToHex(R,G,B) 
{
  return toHex(R)+toHex(G)+toHex(B);
}
function toHex(n) 
{
   n = parseInt(n,10);
   if (isNaN(n)) return "00";
   n = Math.max(0,Math.min(n,255));
   return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
}
function Mensaje(Titulo, Mensaje)
{
  $.gritter.add({
        title: Titulo,
        text: Mensaje
      });
}
var keyStr = "ABCDEFGHIJKLMNOP" +
               "QRSTUVWXYZabcdef" +
               "ghijklmnopqrstuv" +
               "wxyz0123456789+/" +
              "=";
function decode64(input) 
{
   var output = "";
   var chr1, chr2, chr3 = "";
   var enc1, enc2, enc3, enc4 = "";
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   var base64test = /[^A-Za-z0-9\+\/\=]/g;
   if (base64test.exec(input)) 
   {
      
   }
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do 
   {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) 
      {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) 
      {
         output = output + String.fromCharCode(chr3);
      }

      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";

   } while (i < input.length);

   return unescape(output);
}
function encode64(input) {
     input = escape(input);
     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
           enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
           enc4 = 64;
        }

        output = output +
           keyStr.charAt(enc1) +
           keyStr.charAt(enc2) +
           keyStr.charAt(enc3) +
           keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
     } while (i < input.length);

     return output;
  }
function iniciarFinder(ruta, rutaInicial, objeto) 
{
  ruta = 'Archivos/php/connector.php?Contrato=' + ruta + '&rutaInicial=' + rutaInicial;
  
  var elf = $(objeto).elfinder({
    url :   ruta
     ,lang: 'es'
     ,height: 600
     ,commands : [
    'open', 'reload', 'home', 'up', 'back', 'forward', 'getfile', 'quicklook', 
    'download', 'archive', 'info', 'view', 'resize', 'sort'
      ]
    , rememberLastDir: false
    , defaultView : 'list'
  }).elfinder('instance');
/*
  elf.bind('upload', function(e) 
    { /*
      $.each(e.data.added, function(index, value)
        {
          console.log(decode64(value.phash.substr(3)));
          console.log(value.name);
          
        });*/
    /*  
    });
  elf.bind('remove', function(e) 
    { 
      //console.log(e);
    });*/

}
function expirarSesion()
{
  var objDate = 16;
  var sessionFlag = true;

  if (Usuario == null)
  {
    sessionFlag = false;
  } else
  {
    var objUser = JSON.parse(localStorage.getItem('MinTIC_HD'));
    var cDate = new Date();
  
    var pDate = new Date(objUser.cDate);
  
    objDate = cDate - pDate;  
  }

    if (Math.round((objDate/1000)/60) < 30 && sessionFlag)
    {
      objUser.cDate = cDate;
      localStorage.setItem("MinTIC_HD", JSON.stringify(objUser));    
    } else
    {
      if (objUser.id != 94)
      {
        delete localStorage.MinTIC_HD;
        window.location.replace("index.html");
      }
    } 
}
function validarFormulario(objForm)
{
  var rta = true;
  var obj = $(objForm).find(':required');
  $.each(obj, function(index, val) 
  {
     if ($(val).val() == null || $(val).val() == "")
     {
        Mensaje("Error,", "El campo " + $(val).attr("placeholder") + " es obligatorio");
        $(val).focus();
        rta = false;
        return rta;
     }
  });
  return rta;
}
function cerrarMenu()
{
  if ($('#sidebar > ul').is(":visible") === true) {
      $('#main-content').animate({
          'margin-left': '25px'
      });

      $('#sidebar').animate({
          'margin-left': '-190px'
      }, {
          complete: function () {
              $('#sidebar > ul').hide();
              $("#container").addClass("sidebar-closed");
          }
      });
  } else {
      $('#main-content').animate({
          'margin-left': '215px'
      });
      $('#sidebar > ul').show();
      $('#sidebar').animate({
          'margin-left': '0'
      }, {
          complete: function () {
              $("#container").removeClass("sidebar-closed");
          }
      });
  }
}