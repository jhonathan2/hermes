function arranqueHome()
{
  app.sinBackButton();
 
  $("#pnlUsuario span").text(Usuario.Nombre);
  $("#pnlUsuario img").attr("src", "http://hd.wspcolombia.com/" + Usuario.Imagen);

  odbc.ejecutarSQL("SELECT fecha FROM versionBD", [], 
            function(rs)
            { 
              $("#lblUltimaActualizacion span").text(rs[0].fecha)
            });

  $("#btnHome_Sincronizar").on("click", sincronizacionProgramada);
  $("#btnHome_CerrarSesion").on("click", function(evento)
    {
      evento.preventDefault();
      app.cerrarSesion();
    });
}
