function arranqueBeneficiario()
{
  app.sinBackButton();

  var idBeneficiario = parseInt(GET('id'));
  $("#txtBeneficiario_Id span").text(idBeneficiario);

  $.post("php/cargarBeneficiario.php", {idBeneficiario : idBeneficiario}, 
            function(rs)
            { 
              var Beneficiario = rs[0];
              $("#txtBeneficiario_Id span").text(Beneficiario.idBeneficiario);
              $("#txtBeneficiario_Nombre span").text(Beneficiario.Nombre);
              $("#txtBeneficiario_Cedula span").text(Beneficiario.Documento);
              $("#txtBeneficiario_Direccion span").text(Beneficiario.Direccion);
              $("#txtBeneficiario_Barrio span").text(Beneficiario.Barrio);
              $("#txtBeneficiario_Telefono a").text(Beneficiario.Telefono);
              $("#txtBeneficiario_Telefono a").attr("href", "tel:" + Beneficiario.Telefono);
              $("#txtBeneficiario_Celular a").text(Beneficiario.Celular);
              $("#txtBeneficiario_Celular a").attr("href", "tel:" + Beneficiario.Celular);
              $("#txtBeneficiario_Estrato span").text(Beneficiario.Estrato);
              $("#txtBeneficiario_Departamento span").text(Beneficiario.Departamento);
              $("#txtBeneficiario_Municipio span").text(Beneficiario.Municipio);
              $("#txtBeneficiario_Correo a").text(Beneficiario.Correo);
              $("#txtBeneficiario_Correo a").attr("href", "mailto:" + Beneficiario.Correo);
              $("#txtBeneficiario_Genero span").text(Beneficiario.Genero);

              $("#btnIniciarioEncuesta").attr("href", "encuesta.html?id=" + idBeneficiario);
              
            }, "json");
}

