function arranqueProgramacion()
{

  $.post("php/cargarProgramacion.php", {}, 
      function(rs)
      { 
        $("#lstProgramacion_Listado li").remove();
        var tds = "";
            
        $.each(rs, function(index, value)
          {
            tds += '<li data-filtertext="'+ value.Nombre + ' ' + value.Municipio + ' ' + value.Barrio + ' ' + value.Direccion +'">';
            tds +=  '<a href="beneficiario.html?id=' + value.idBeneficiario + '" target="_self">';
            tds +=    '<strong>' + value.Nombre + '</strong>';
            tds +=    '<br><small>' + value.Municipio + ' ' + value.Direccion + '</small>';
            tds +=  '</a>';
            tds += '</li>';
          });
        
        $("#lstProgramacion_Listado").append(tds);
        $("#lstProgramacion_Listado").filterable( "refresh" );
        
      }, "json");
}
