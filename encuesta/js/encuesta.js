function encuesta() 
{
  var idBeneficiario = parseInt(GET("id"));
    $("#txtEncuesta_Prefijo").val(obtenerPrefijo());
    $("#txtEncuesta_FechaIni").val(obtenerFecha());
    
    $("#btnEncuesta_Cerrar").attr("idBeneficiario", idBeneficiario);
    $("#btnEncuesta_Cerrar").on("click", btnEncuesta_Cerrar_Click);

    $(".btnIniciarItem").on("click", function()
      {
        $(this).find(".Encuesta_Ok").remove();
      });

    $(".encuesta-link").on("click", evaluarPagina);
}

function evaluarPagina(evento)
{
  evento.preventDefault();
  objContenedor = $(this).parent("div").parent("div").attr("id");
  var objListado = $("#" + objContenedor).find(".encuesta_item");
  var Evaluacion = true;
  $.each(objListado, function(index, val) 
  {
    
     if ($(val).attr('required') != null)
     {
        if ($(val).attr("encuesta-tipo") == "radio")
        {
          if ($(val).find("input:checked").val() == null)
          {
            Mensaje("Error", "El campo " + $(val).attr("placeholder") + ", debe ser diligenciado");
            $(val).focus();
            Evaluacion = false;
          }
        } else
        {
          if ($(val).val() === null || $(val).val() === "")
          {
            Mensaje("Error", "El campo " + $(val).attr("placeholder") + ", debe ser diligenciado");
            $(val).focus();
            Evaluacion = false;
          }
        }
     }
     return Evaluacion;
  });
  if (Evaluacion)
  {
    if ($(this).attr("ultimo") != null)
    {
      
      var idBtn = $(this).attr("ultimo");
      
      $("#" + idBtn).append('<div class="Encuesta_Ok">Ok</div>');
      var obj = $(".Encuesta_Ok");
      if (obj.length == 3)
      {
        $("#lblEncuesta_Estado strong").text("Completa");
        $("#lblEncuesta_Estado strong").css("color", "green");
      } else
      {
        $("#lblEncuesta_Estado strong").text("Incompleta");
        $("#lblEncuesta_Estado strong").css("color", "red");
      }
    }   
    $.mobile.changePage($(this).attr("encuesta-direccion"));

  }
}
function btnEncuesta_Cerrar_Click(evento)
{
  evento.preventDefault();
  var objListado = $(".encuesta_item");

  var Resultado = "";
  $.each(objListado, function(index, val) 
  {
    var Valor = 1;
    if ($(val).attr("encuesta-tipo") == "radio")
    {
      if ($(val).find("input:checked").val() != null)
      {
        Valor = $(val).find("input:checked").val();
      }
    } else
    {
      Valor = $(val).val();
    }
    Resultado += $(val).attr("encuesta-numeral") + "->" + Valor + "##";
  });
  
  var idBeneficiario = parseInt($(this).attr("idBeneficiario"));
  var fecha = obtenerFecha();
    var Usuario = JSON.parse(localStorage.getItem('MinTIC_HD'));
    var obj = [
      {
        Prefijo : $("#txtEncuesta_Prefijo").val(),
        idProgramacion : idBeneficiario,
        fechaInicio : $("#txtEncuesta_FechaIni").val(),
        fechaFin : fecha,
        idLogin : Usuario.id,
        Resultado : Resultado,
        CoordenadasIni : "Telefonica",
        CoordenadasFin : "Telefonica",
      }];
    /*
    obj[0].Prefijo = $("#txtEncuesta_Prefijo").val();
    obj[0].idProgramacion = idBeneficiario;
    obj[0].fechaInicio = $("#txtEncuesta_FechaIni").val();
    obj[0].fechaFin = fecha;
    obj[0].idLogin = Usuario.id;
    obj[0].Resultado = Resultado;
    obj[0].CoordenadasIni = "Telefonica";
    obj[0].CoordenadasFin = "Telefonica";*/

  $.post("../movil/php/sincronizarEncuestas.php", {datos : obj}, function(data)
  {
    window.location.replace("programacion.html");
  }, "json");
}