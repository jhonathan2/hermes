<?php
	include("../../php/conectar.php"); 
   $link = Conectar();

   $sql = "SELECT idBeneficiario, Nombre, CodDane_Municipios.NomMunicipio, Barrio, Direccion FROM Beneficiarios INNER JOIN CodDane_Municipios ON CodDane_Municipios.CodMunicipio = Beneficiarios.Municipio AND CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento WHERE Estado <> 'Ejecutado';";
   
   $result = $link->query($sql);

   if ($result->num_rows > 0)
   {
      class Beneficiario
      {
         public $idBeneficiario;
         public $Nombre;
         public $Municipio;
         public $Barrio;
         public $Direccion;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Municipios[$idx] = new Beneficiario();
            $Municipios[$idx]->idBeneficiario = utf8_encode($row['idBeneficiario']);
            $Municipios[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Municipios[$idx]->Municipio = utf8_encode($row['Municipio']);
            $Municipios[$idx]->Barrio = utf8_encode($row['Barrio']);
            $Municipios[$idx]->Direccion = utf8_encode($row['Direccion']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Municipios);   
   } else
   {
      echo 0;
   }
?>