<?php
  include("../../php/conectar.php"); 
   $link = Conectar();

   $idBeneficiario = $_POST['idBeneficiario'];

   $sql = "SELECT 
            Beneficiarios.idBeneficiario, 
            Beneficiarios.Nombre, 
            Beneficiarios.Documento,
            Beneficiarios.Direccion,
            Beneficiarios.Barrio,
            Beneficiarios.Telefono,
            Beneficiarios.Celular,
            Beneficiarios.Estrato,
            CodDane_Departamentos.Departamento, 
            CodDane_Municipios.NomMunicipio, 
            Beneficiarios.Correo,
            Beneficiarios.Genero            
          FROM 
            Beneficiarios 
            INNER JOIN CodDane_Municipios ON 
              CodDane_Municipios.CodMunicipio = Beneficiarios.Municipio 
              AND CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento 
            INNER JOIN CodDane_Departamentos ON 
              CodDane_Departamentos.Codigo = Beneficiarios.Departamento 
          WHERE idBeneficiario = '$idBeneficiario';";
   
   $result = $link->query($sql);

   if ($result->num_rows > 0)
   {
      class Beneficiario
      {
        public $idBeneficiario;
        public $Nombre;
        public $Documento;
        public $Direccion;
        public $Barrio;
        public $Telefono;
        public $Celular;
        public $Estrato;
        public $Departamento;
        public $Municipio;
        public $Correo;
        public $Genero;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Beneficiario[$idx] = new Beneficiario();
            $Beneficiario[$idx]->idBeneficiario = utf8_encode($row['idBeneficiario']);
            $Beneficiario[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Beneficiario[$idx]->Documento = utf8_encode($row['Documento']);
            $Beneficiario[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Beneficiario[$idx]->Barrio = utf8_encode($row['Barrio']);
            $Beneficiario[$idx]->Telefono = utf8_encode($row['Telefono']);
            $Beneficiario[$idx]->Celular = utf8_encode($row['Celular']);
            $Beneficiario[$idx]->Estrato = utf8_encode($row['Estrato']);
            $Beneficiario[$idx]->Departamento = utf8_encode($row['Departamento']);
            $Beneficiario[$idx]->Municipio = utf8_encode($row['Municipio']);
            $Beneficiario[$idx]->Correo = utf8_encode($row['Correo']);
            $Beneficiario[$idx]->Genero = utf8_encode($row['Genero']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Beneficiario);   
   } else
   {
      echo 0;
   }
?>