<?php
   $idx = 0;
   $contador = 0;
   
   include_once("conectar.php"); 
   $link = Conectar();

   $sql = array();
   $sql[0] = "";

   $sql2 = array();
   $sql2[0] = "";

   $_sql = "DELETE FROM archivosCargados;";
   $result = $link->query($_sql);



   listar_directorios_ruta("../Archivos/files/hd/");
   InsertarRegistros();
   clasificarArchivos();
   
function listar_directorios_ruta($ruta)
{ 
   // abrir un directorio y listarlo recursivo 
   global $idx, $contador, $sql, $sql2, $link;
   if (is_dir($ruta)) 
   { 
      if ($dh = opendir($ruta)) 
      { 
         while (($file = readdir($dh)) !== false) 
         { 
            if ( $file!="." && $file!="..")
            {
               /*
                  $strFile = limpiarCaracteresEspeciales($file);
                  rename($ruta . $file, $ruta .  $strFile);
                  $file = $strFile;
               */

               if (is_dir($ruta . $file))
               { 
                  listar_directorios_ruta($ruta .  $file . "/"); 
               }
               else
               {
                  $sql[$idx] .= "('" . utf8_encode(substr($ruta, 18, -1)) . "', '" . utf8_encode($file) . "', '" . filesize($ruta . $file) . "', '" .  substr($file, -4) . "'), "; 
                  $sql2[$idx] .= "('Sin Descripción', '" . utf8_encode(substr($ruta, 18, -1)) . "', '" . utf8_encode($file) . "'), "; 
                  //echo "<br>('" . substr($ruta, 18, -1) . "', '$file', '" . filesize($ruta . $file) . "', '" .  substr($file, -4) . "'), "; 
                  
                  $contador++;
                  if ($contador == 999)
                  {
                     $contador = 0;
                     $idx++;
                     $sql[$idx] = "";
                  }
               }
            }
         } 
      closedir($dh); 

      } 
   }else 
   {
      echo "<br>No es ruta valida";       
   }
   
} 
function InsertarRegistros()
{
   global $idx, $contador, $sql, $sql2, $link;
   if ($contador > 0 OR $idx > 0)
   {
      $values2 = "";
      foreach ($sql as $key => $value) 
      {
         $strSql = "INSERT INTO archivosCargados (Ruta, Nombre, peso, extension) VALUES ";
         $strSql .= substr($value, 0, -2);
         $strSql .= " ON DUPLICATE KEY UPDATE 
         Ruta = VALUES(Ruta),
         Nombre = VALUES(Nombre),
         peso = VALUES(peso),
         extension = VALUES(extension);";

         
         $result = $link->query(utf8_decode($strSql));
         $values2 .= $sql2[$key];
      }
      $objSql = "INSERT INTO Documentos (Ruta, NomArchivo) VALUES " . substr($values2, 0, -2);
      $objSql .= "ON DUPLICATE KEY UPDATE 
         Ruta = VALUES(Ruta),
         NomArchivo = VALUES(NomArchivo);";

      $result = $link->query(utf8_decode($objSql));
   }
}
function limpiarCaracteresEspeciales($string )
{
 $string = htmlentities($string);
 $string = preg_replace('/\&(.)[^;]*;/', '\\1', $string);
 return $string;
}
function clasificarArchivos()
{
   global $link;

   $_sql = "UPDATE Documentos SET Estado = 'Ok' WHERE 1";
   $link->query($_sql);

   $_sql = "SELECT idDocumento FROM Documentos LEFT JOIN archivosCargados on archivosCargados.Ruta = Documentos.Ruta AND archivosCargados.Nombre = Documentos.NomArchivo WHERE idArchivo IS NULL;";
   $result = $link->query($_sql);

   $idDocumento = "";
   while ($row = mysqli_fetch_assoc($result))
   { 
      $idDocumento .= $row['idDocumento'] . ", ";
   }
   if ($idDocumento <> "")
   {
      $idDocumento = substr($idDocumento, 0, -2);
      $_sql = "UPDATE Documentos SET Estado = 'Huerfano' WHERE idDocumento IN ($idDocumento);";
      $result = $link->query($_sql);
   }

   $_sql = "SELECT idArchivo FROM archivosCargados LEFT JOIN  Documentos on archivosCargados.Ruta = Documentos.Ruta AND archivosCargados.Nombre = Documentos.NomArchivo WHERE Documentos.Asunto IS NULL OR Documentos.Asunto LIKE ''";
   $result = $link->query($_sql);

   $idDocumento = "";
   while ($row = mysqli_fetch_assoc($result))
   { 
      $idDocumento .= $row['idArchivo'] . ", ";
   }
   if ($idDocumento <> "")
   {
      $idDocumento = substr($idDocumento, 0, -2);
      $_sql = "UPDATE archivosCargados SET Estado = 'Sin Asociar' WHERE idArchivo IN ($idDocumento);";
      $result = $link->query($_sql);
   }
}
?>
