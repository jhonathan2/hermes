<?php

	include("conectar.php"); 
   $link = Conectar();

   $datos = $_POST['datos'];

   $condicion = "1 ";
   if ($datos['fechaIni'] <> "")
   {
      $condicion .= "AND encuestas.fechaInicio >= '" . $datos['fechaIni'] . "' ";
   }
   if ($datos['fechaFin'] <> "")
   {
      $condicion .= "AND encuestas.fechaInicio <= '" . $datos['fechaFin'] . "' ";  
   }
   if ($datos['CodBeneficiario'] <> "")
   {
      $condicion .= "AND encuestas.idBeneficario LIKE '%" . $datos['CodBeneficiario'] . "%' ";  
   }
   if ($datos['Departamento'] <> "")
   {
      $condicion .= "AND CodDane_Departamentos.Departamento LIKE '%" . $datos['Departamento'] . "%' ";  
   }
   if ($datos['Municipio'] <> "")
   {
      $condicion .= "AND CodDane_Municipios.NomMunicipio LIKE '%" . $datos['Municipio'] . "%' ";  
   }
   if ($datos['Direccion'] <> "")
   {
      $condicion .= "AND Beneficiarios.Direccion LIKE '%" . $datos['Direccion'] . "%' ";  
   }

   if ($condicion <> "1 ")
   {
      $condicion = substr($condicion, 5);
   }


   $sql = "SELECT 
            encuestas.Prefijo,
            encuestas.fechaInicio,
            encuestas.fechaFin,
            CodDane_Departamentos.Departamento,
            CodDane_Municipios.NomMunicipio,
            CONCAT(Beneficiarios.Departamento, Beneficiarios.Municipio) AS 'CodDane',
            DatosUsuarios.Nombre AS 'Usuario',
            Beneficiarios.Contrato,
            Beneficiarios.Nombre AS 'BeneficiarioNombre',
            Beneficiarios.Documento AS 'BeneficiarioDocumento',
            encuestas.idBeneficario AS 'BeneficiarioId',
            Beneficiarios.Direccion AS 'BeneficiarioDireccion',
            Beneficiarios.Barrio AS 'BeneficiarioBarrio',
            Beneficiarios.Telefono AS 'BeneficiarioTelefono',
            Beneficiarios.Celular AS 'BeneficiarioCelular',
            Beneficiarios.Estrato AS 'BeneficiarioEstrato',
            Beneficiarios.Correo AS 'BeneficiarioCorreo',
            Beneficiarios.Genero AS 'BeneficiarioGenero',
            encuestas.Resultado
         FROM 
            encuestas 
            INNER JOIN DatosUsuarios ON DatosUsuarios.idLogin = encuestas.idLogin
            INNER JOIN Beneficiarios ON Beneficiarios.idBeneficiario = encuestas.idBeneficario
            INNER JOIN CodDane_Departamentos ON CodDane_Departamentos.Codigo = Beneficiarios.Departamento
            INNER JOIN CodDane_Municipios ON CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento AND CodDane_Municipios.CodMunicipio =  Beneficiarios.Municipio
         WHERE $condicion;";
   
   $result = $link->query($sql);

   if ($result->num_rows > 0)
   {
            
      $idx = 0;
      $idy = 0;
      $idBeneficiario = 0;

      $Resultados = array();
         while ($row = mysqli_fetch_assoc($result))
         { 
            $idBeneficiario = $row['BeneficiarioId'];
            $Resultados[$row['BeneficiarioId']] = array();
            $Resultados[$row['BeneficiarioId']]['Prefijo'] = $row['Prefijo'];
            $Resultados[$row['BeneficiarioId']]['idBeneficiario'] = $row['BeneficiarioId'];
            $Resultados[$row['BeneficiarioId']]['fechaInicio'] = $row['fechaInicio'];
            $Resultados[$row['BeneficiarioId']]['fechaFin'] = $row['fechaFin'];
            $Resultados[$row['BeneficiarioId']]['Departamento'] = $row['Departamento'];
            $Resultados[$row['BeneficiarioId']]['NomMunicipio'] = $row['NomMunicipio'];
            $Resultados[$row['BeneficiarioId']]['CodDane'] = $row['CodDane'];
            $Resultados[$row['BeneficiarioId']]['Usuario'] = $row['Usuario'];
            $Resultados[$row['BeneficiarioId']]['BeneficiarioNombre'] = trim($row['BeneficiarioNombre']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioDocumento'] = trim($row['BeneficiarioDocumento']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioDireccion'] = trim($row['BeneficiarioDireccion']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioBarrio'] = trim($row['BeneficiarioBarrio']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioTelefono'] = trim($row['BeneficiarioTelefono']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioCelular'] = trim($row['BeneficiarioCelular']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioEstrato'] = trim($row['BeneficiarioEstrato']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioCorreo'] = trim($row['BeneficiarioCorreo']);
            $Resultados[$row['BeneficiarioId']]['BeneficiarioGenero'] = trim($row['BeneficiarioGenero']);
            
            $tmpResultado = explode("##", $row['Resultado']);

            foreach ($tmpResultado as $key => $value) 
            {
               if ($value <> "")
               {
                  $tmpValor = explode('->', $value);
                  if ($tmpValor[0] == "5_4")
                  {
                     $tmpValor[0] = "5_" . (4 + $idy);
                     $idy++;
                  } else
                  {
                     $idy = 0;
                  }
                  $Resultados[$row['BeneficiarioId']][$tmpValor[0]] = $tmpValor[1];
               }
            }

            $idx++;
         }

         /****************** Obtener Archivo de Resultados ******/
            $Encabezado = "<tr>";
            $Salida = "";
            foreach ($Resultados[$idBeneficiario] as $key => $value) 
            {
                  $Encabezado .= "<th>" . $key . "</th>";
            }
            $Encabezado .= "</tr>";

            foreach ($Resultados as $key => $val) 
            {
               $Salida .= "<tr>";
               foreach ($val as $key => $value) 
               {
                  if ($key == "Prefijo")
                  {
                     $Salida .= "<td>'" . $value . "</td>";
                  } else
                  {
                     $Salida .= "<td>" . $value . "</td>";  
                  }
               }
               $Salida .= "</tr>";
            }
               mysqli_free_result($result);
            
            class Excel
            {
               public $Encabezado;
               public $Cuerpo;
            }

            $Respuesta = new Excel();
            $Respuesta->Encabezado = $Encabezado;
            $Respuesta->Cuerpo = $Salida;

            echo json_encode($Respuesta);

         /*********************************************************/
         
   } else
   {
      echo 0;
   }
?>