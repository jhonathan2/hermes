<?php
	include("conectar.php"); 
   $link = Conectar();

   $Parametro = addslashes($_POST['Parametro']);

   $Parametro = str_replace(" ", "%", $Parametro);

   $sql = "SELECT *  FROM Documentos WHERE 
            Estado = 'Ok'
            AND (Tipo LIKE '%" . $Parametro . "%' 
             OR Operador LIKE '%" . $Parametro . "%' 
             OR Consecutivo LIKE '%" . $Parametro . "%' 
             OR Ref_1 LIKE '%" . $Parametro . "%' 
             OR Ref_2 LIKE '%" . $Parametro . "%' 
             OR Ref_3 LIKE '%" . $Parametro . "%' 
             OR Asunto LIKE '%" . $Parametro . "%' 
             OR Keywords LIKE '%" . $Parametro . "%' 
             OR Responsable LIKE '%" . $Parametro . "%' 
             OR Anexos LIKE '%" . $Parametro . "%' 
             OR Scan LIKE '%" . $Parametro . "%' 
             OR NomArchivo LIKE '%" . $Parametro . "%')";   
   
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Documento
      {
         public $Ruta;
         public $Nombre;
         public $Asunto;

         public $Tipo;
         public $Operador;
         public $Consecutivo;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Documentos[$idx] = new Documento();
            $Documentos[$idx]->Ruta = utf8_encode($row['Ruta']);
            $Documentos[$idx]->Nombre = utf8_encode($row['NomArchivo']);
            $Documentos[$idx]->Asunto = utf8_encode($row['Asunto']);

            $Documentos[$idx]->Tipo = utf8_encode($row['Tipo']);
            $Documentos[$idx]->Operador = utf8_encode($row['Operador']);
            $Documentos[$idx]->Consecutivo = utf8_encode($row['Consecutivo']);            

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Documentos);   
   } else
   {
      echo 0;
   }
?>