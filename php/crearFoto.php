<?php 
function crearFoto($ruta, $imagen)
{	
	date_default_timezone_set('America/Bogota');
	
 	$imagen64 = str_replace(" ", "+", $imagen);

 	
 	$Nombre = $ruta . date('Y_m_d_h_i_s'). ".png";
 	//$Nombre = $Prefijo . "_PDAImagen_" . date('Y_m_d_h_i_s'). ".png";

	$imagen64 = explode(",", $imagen64);

 	$imagen64[1] = base64_decode($imagen64[1]);
	$imagen64[0] = str_replace("data", "Content-Type", $imagen64[0]);

	$im = imagecreatefromstring($imagen64[1]);
	if ($im != false) 
	{
    	header('Content-Type: image/png');
    	imagepng($im, "../" . $Nombre, 5);
    	imagedestroy($im);
	}
	return $Nombre;
}
?> 
