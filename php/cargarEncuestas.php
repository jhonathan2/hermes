<?php
	include("conectar.php"); 
   $link = Conectar();

   $datos = $_POST['datos'];

   $condicion = "1 ";
   if ($datos['fechaIni'] <> "")
   {
      $condicion .= "AND Fecha >= '" . $datos['fechaIni'] . "' ";
   }
   if ($datos['fechaFin'] <> "")
   {
      $condicion .= "AND Fecha <= '" . $datos['fechaFin'] . "' ";  
   }
   if ($datos['CodBeneficiario'] <> "")
   {
      $condicion .= "AND idBeneficario LIKE '%" . $datos['CodBeneficiario'] . "%' ";  
   }
   if ($datos['Departamento'] <> "")
   {
      $condicion .= "AND Departamento LIKE '%" . $datos['Departamento'] . "%' ";  
   }
   if ($datos['Municipio'] <> "")
   {
      $condicion .= "AND NomMunicipio LIKE '%" . $datos['Municipio'] . "%' ";  
   }
   if ($datos['Direccion'] <> "")
   {
      $condicion .= "AND Direccion LIKE '%" . $datos['Direccion'] . "%' ";  
   }
   if ($datos['Tipo'] <> "")
   {
      $condicion .= "AND Tipo = '" . $datos['Tipo'] . "' ";  
   }

   if ($condicion <> "1 ")
   {
      $condicion = substr($condicion, 5);
   }


   $sql = "SELECT *  FROM tblEncuestas WHERE $condicion";
   $result = $link->query($sql);

   if ($result->num_rows > 0)
   {
      class Resultado
      {
         public $idBeneficiario;
         public $Fecha;
         public $Hora;
         public $Departamento;
         public $NomMunicipio;
         public $Direccion;
         public $Coordenadas;
         public $Tipo;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Resultados[$idx] = new Resultado();
            $Resultados[$idx]->idBeneficiario = utf8_encode($row['idBeneficario']);
            $Resultados[$idx]->Fecha = utf8_encode($row['Fecha']);
            $Resultados[$idx]->Hora = utf8_encode($row['Hora']);
            $Resultados[$idx]->Departamento = utf8_encode($row['Departamento']);
            $Resultados[$idx]->NomMunicipio = utf8_encode($row['NomMunicipio']);
            $Resultados[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Resultados[$idx]->Coordenadas = utf8_encode($row['Coordenadas']);
            $Resultados[$idx]->Tipo = utf8_encode($row['Tipo']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Resultados);   
   } else
   {
      echo 0;
   }
?>