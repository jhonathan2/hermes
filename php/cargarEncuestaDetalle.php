<?php
	include_once "conectar.php"; 

   function cargarDatos($idBeneficiario)
   {
      $link = Conectar();
      $sql = "SELECT 
               encuestas.Prefijo,
               encuestas.fechaInicio,
               encuestas.fechaFin,
               CodDane_Departamentos.Departamento,
               CodDane_Municipios.NomMunicipio,
               CONCAT(Beneficiarios.Departamento, Beneficiarios.Municipio) AS 'CodDane',
               DatosUsuarios.Nombre AS 'Usuario',
               Beneficiarios.Contrato,
               Beneficiarios.Nombre AS 'BeneficiarioNombre',
               Beneficiarios.Documento AS 'BeneficiarioDocumento',
               encuestas.idBeneficario AS 'BeneficiarioId',
               Beneficiarios.Direccion AS 'BeneficiarioDireccion',
               Beneficiarios.Barrio AS 'BeneficiarioBarrio',
               Beneficiarios.Telefono AS 'BeneficiarioTelefono',
               Beneficiarios.Celular AS 'BeneficiarioCelular',
               Beneficiarios.Estrato AS 'BeneficiarioEstrato',
               Beneficiarios.Correo AS 'BeneficiarioCorreo',
               Beneficiarios.Genero AS 'BeneficiarioGenero',
               encuestas.Resultado
            FROM 
               encuestas 
               INNER JOIN DatosUsuarios ON DatosUsuarios.idLogin = encuestas.idLogin
               INNER JOIN Beneficiarios ON Beneficiarios.idBeneficiario = encuestas.idBeneficario
               INNER JOIN CodDane_Departamentos ON CodDane_Departamentos.Codigo = Beneficiarios.Departamento
               INNER JOIN CodDane_Municipios ON CodDane_Municipios.CodDepartamento = Beneficiarios.Departamento AND CodDane_Municipios.CodMunicipio =  Beneficiarios.Municipio
            WHERE encuestas.idBeneficario = '$idBeneficiario'";
      
      $result = $link->query($sql);
   
      if ($result->num_rows > 0)
      {
               
         $idx = 0;
         $idy = 0;
         $idBeneficiario = 0;
   
         $Resultados = array();
            while ($row = mysqli_fetch_assoc($result))
            { 
               $Resultados['Prefijo'] = $row['Prefijo'];
               $Resultados['idBeneficiario'] = $row['BeneficiarioId'];
               $Resultados['fechaInicio'] = $row['fechaInicio'];
               $Resultados['fechaFin'] = $row['fechaFin'];
               $Resultados['Departamento'] = $row['Departamento'];
               $Resultados['NomMunicipio'] = $row['NomMunicipio'];
               $Resultados['CodDane'] = $row['CodDane'];
               $Resultados['Usuario'] = $row['Usuario'];
               $Resultados['BeneficiarioNombre'] = trim($row['BeneficiarioNombre']);
               $Resultados['BeneficiarioDocumento'] = trim($row['BeneficiarioDocumento']);
               $Resultados['BeneficiarioContrato'] = trim($row['Contrato']);
               $Resultados['BeneficiarioDireccion'] = trim($row['BeneficiarioDireccion']);
               $Resultados['BeneficiarioBarrio'] = trim($row['BeneficiarioBarrio']);
               $Resultados['BeneficiarioTelefono'] = trim($row['BeneficiarioTelefono']);
               $Resultados['BeneficiarioCelular'] = trim($row['BeneficiarioCelular']);
               $Resultados['BeneficiarioEstrato'] = trim($row['BeneficiarioEstrato']);
               $Resultados['BeneficiarioCorreo'] = trim($row['BeneficiarioCorreo']);
               $Resultados['BeneficiarioGenero'] = trim($row['BeneficiarioGenero']);
               
               $tmpResultado = explode("##", $row['Resultado']);
   
               foreach ($tmpResultado as $key => $value) 
               {
                  if ($value <> "")
                  {
                     $tmpValor = explode('->', $value);
                     if ($tmpValor[0] == "5_4")
                     {
                        $tmpValor[0] = "5_" . (4 + $idy);
                        $idy++;
                     } else
                     {
                        $idy = 0;
                     }
                     $Resultados[$tmpValor[0]] = utf8_decode($tmpValor[1]);
                  }
               }
   
               $idx++;
            }
               return $Resultados;
   
               mysqli_free_result($result);  
      } else
      {
         return 0;
      }
   }
?>