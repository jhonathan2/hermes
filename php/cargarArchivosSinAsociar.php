<?php

  date_default_timezone_set('America/Bogota');
	include("conectar.php"); 
   $link = Conectar();

   $sql = "SELECT *  FROM archivosCargados WHERE Estado = 'Sin Asociar';";   
   /*$sql = "SELECT 
            idDocumento AS idArchivo,
            Ruta,
            NomArchivo AS Nombre,
            0 AS peso,
            'pdf' AS extension
         FROM Documentos WHERE Descripcion = '';";   */
   
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Documento
      {
         public $idArchivo;
         public $Ruta;
         public $Nombre;
         public $Peso;
         public $Extension;
         public $Fecha;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Documentos[$idx] = new Documento();
            $Documentos[$idx]->idArchivo = utf8_encode($row['idArchivo']);
            $Documentos[$idx]->Ruta = utf8_encode($row['Ruta']);
            $Documentos[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Documentos[$idx]->Peso = utf8_encode($row['peso']);
            $Documentos[$idx]->Extension = utf8_encode($row['extension']);
            //$Documentos[$idx]->Fecha = date ("Y-m-d Y H:i:s.", filemtime("../Archivos/files/" . $row['Ruta'] . "/" . $row['Nombre']));
            $Documentos[$idx]->Fecha = "";


            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Documentos);   
   } else
   {
      echo 0;
   }
?>