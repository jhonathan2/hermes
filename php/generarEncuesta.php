<?php
require('fpdf.php');
require('cargarEncuestaDetalle.php');

$idBeneficiario = $_GET['id'];

$Valores = cargarDatos($idBeneficiario);

	$pValores = array();

	$pValores['AM'] = 0;
	$pValores['PM'] = 0;

	if ((substr($Valores['fechaInicio'], 11, 2) * 1) > 12)
	{
		$pValores['PM'] = 1;
	} else
	{
		$pValores['AM'] = 1;
	}


	$pValores[518] = 0;
	$pValores[519] = 0;
	$pValores[520] = 0;
	$pValores[$Valores['BeneficiarioContrato']] = 1;

	$pValores['CorreoSI'] = 0;
	$pValores['CorreoNO'] = 1;

	if (trim($Valores['BeneficiarioCorreo']) <> "")
	{
		$pValores['CorreoSI'] = 1;
		$pValores['CorreoNO'] = 0;
	}

	$pValores['2_12_F'] = 0;
	$pValores['2_12_M'] = 0;
	$pValores['2_12_' . $Valores['BeneficiarioGenero']] = 1;

	$pValores['3_2_1'] = 0;
	$pValores['3_2_2'] = 0;
	$pValores['3_2_3'] = 0;
	$pValores['3_2_4'] = 0;
	$pValores['3_2_5'] = 0;
	$pValores['3_2_' . $Valores['3_2']] = 1;

	$pValores['3_CorreoSI'] = 0;
	$pValores['3_CorreoNO'] = 1;

	if (trim($Valores['3_3']) <> "")
	{
		$pValores['3_CorreoSI'] = 1;
		$pValores['3_CorreoNO'] = 0;
	}

	$pValores['3_4_1'] = 0;
	$pValores['3_4_2'] = 0;
	$pValores['3_4_' . $Valores['3_4']] = 1;

	$pValores['3_6_1'] = 0;
	$pValores['3_6_2'] = 0;
	$pValores['3_6_3'] = 0;
	$pValores['3_6_4'] = 0;
	$pValores['3_6_5'] = 0;
	$pValores['3_6_6'] = 0;
	$pValores['3_6_' . $Valores['3_6']] = 1;

	$pValores['3_7_1'] = 0;
	$pValores['3_7_2'] = 0;
	$pValores['3_7_3'] = 0;
	$pValores['3_7_4'] = 0;
	$pValores['3_7_5'] = 0;
	$pValores['3_7_6'] = 0;
	$pValores['3_7_' . $Valores['3_7']] = 1;

	$pValores['3_8_1'] = 0;
	$pValores['3_8_2'] = 0;
	$pValores['3_8_' . $Valores['3_8']] = 1;

	$pValores['3_9_1'] = 0;
	$pValores['3_9_2'] = 0;
	$pValores['3_9_3'] = 0;
	$pValores['3_9_4'] = 0;
	$pValores['3_9_5'] = 0;
	$pValores['3_9_6'] = 0;
	$pValores['3_9_7'] = 0;
	$pValores['3_9_' . $Valores['3_9']] = 1;

	$pValores['4_1_1'] = 0;
	$pValores['4_1_2'] = 0;
	$pValores['4_1_3'] = 0;
	$pValores['4_1_' . $Valores['4_1']] = 1;

	$pValores['4_2_1'] = 0;
	$pValores['4_2_2'] = 0;
	$pValores['4_2_3'] = 0;
	$pValores['4_2_4'] = 0;
	$pValores['4_2_5'] = 0;
	$pValores['4_2_' . $Valores['4_2']] = 1;

	$pValores['4_3_1'] = 0;
	$pValores['4_3_2'] = 0;
	$pValores['4_3_3'] = 0;
	$pValores['4_3_4'] = 0;
	$pValores['4_3_5'] = 0;
	$pValores['4_3_' . $Valores['4_3']] = 1;

	$pValores['4_4_1'] = 0;
	$pValores['4_4_2'] = 0;
	$pValores['4_4_3'] = 0;
	$pValores['4_4_4'] = 0;
	$pValores['4_4_5'] = 0;
	$pValores['4_4_' . $Valores['4_4']] = 1;

	$pValores['4_5_1'] = 0;
	$pValores['4_5_2'] = 0;
	$pValores['4_5_3'] = 0;
	$pValores['4_5_' . $Valores['4_5']] = 1;

	$pValores['4_6_1'] = 0;
	$pValores['4_6_2'] = 0;
	$pValores['4_6_3'] = 0;
	$pValores['4_6_4'] = 0;
	$pValores['4_6_5'] = 0;
	$pValores['4_6_6'] = 0;
	$pValores['4_6_' . $Valores['4_6']] = 1;

	$pValores['4_7_1'] = 0;
	$pValores['4_7_2'] = 0;
	$pValores['4_7_3'] = 0;
	$pValores['4_7_4'] = 0;
	$pValores['4_7_' . $Valores['4_7']] = 1;

	$pValores['4_8_1'] = 0;
	$pValores['4_8_2'] = 0;
	$pValores['4_8_3'] = 0;
	$pValores['4_8_4'] = 0;
	$pValores['4_8_' . $Valores['4_8']] = 1;

	$pValores['4_9_1'] = 0;
	$pValores['4_9_2'] = 0;
	$pValores['4_9_3'] = 0;
	$pValores['4_9_' . $Valores['4_9']] = 1;

	$pValores['4_10_1'] = 0;
	$pValores['4_10_2'] = 0;
	$pValores['4_10_3'] = 0;
	$pValores['4_10_4'] = 0;
	$pValores['4_10_' . $Valores['4_10']] = 1;

	$pValores['4_11A_1'] = 0;
	$pValores['4_11A_2'] = 0;
	$pValores['4_11A_3'] = 0;
	$pValores['4_11A_4'] = 0;
	$pValores['4_11A_5'] = 0;
	$pValores['4_11A_' . $Valores['4_11A']] = 1;

	$pValores['4_11B_1'] = 0;
	$pValores['4_11B_2'] = 0;
	$pValores['4_11B_3'] = 0;
	$pValores['4_11B_4'] = 0;
	$pValores['4_11B_5'] = 0;
	$pValores['4_11B_' . $Valores['4_11B']] = 1;

	$pValores['4_11C_1'] = 0;
	$pValores['4_11C_2'] = 0;
	$pValores['4_11C_3'] = 0;
	$pValores['4_11C_4'] = 0;
	$pValores['4_11C_5'] = 0;
	$pValores['4_11C_' . $Valores['4_11C']] = 1;

	$pValores['4_11D_1'] = 0;
	$pValores['4_11D_2'] = 0;
	$pValores['4_11D_3'] = 0;
	$pValores['4_11D_4'] = 0;
	$pValores['4_11D_5'] = 0;
	$pValores['4_11D_' . $Valores['4_11D']] = 1;

	$pValores['4_11E_1'] = 0;
	$pValores['4_11E_2'] = 0;
	$pValores['4_11E_3'] = 0;
	$pValores['4_11E_4'] = 0;
	$pValores['4_11E_5'] = 0;
	$pValores['4_11E_' . $Valores['4_11E']] = 1;

	$pValores['4_11F_1'] = 0;
	$pValores['4_11F_2'] = 0;
	$pValores['4_11F_3'] = 0;
	$pValores['4_11F_4'] = 0;
	$pValores['4_11F_5'] = 0;
	$pValores['4_11F_' . $Valores['4_11F']] = 1;

	$pValores['5_1_1'] = 0;
	$pValores['5_1_2'] = 0;
	$pValores['5_1_3'] = 0;
	$pValores['5_1_' . $Valores['5_1']] = 1;

	$pValores['5_2_1'] = 0;
	$pValores['5_2_2'] = 0;
	$pValores['5_2_3'] = 0;
	$pValores['5_2_' . $Valores['5_2']] = 1;

	$pValores['5_3_1'] = 0;
	$pValores['5_3_2'] = 0;
	$pValores['5_3_3'] = 0;
	$pValores['5_3_' . $Valores['5_3']] = 1;

	$pValores['5_4_1'] = 0;
	$pValores['5_4_2'] = 0;
	$pValores['5_4_3'] = 0;
	$pValores['5_4_' . $Valores['5_4']] = 1;

	$pValores['5_5_1'] = 0;
	$pValores['5_5_2'] = 0;
	$pValores['5_5_3'] = 0;
	$pValores['5_5_4'] = 0;
	$pValores['5_5_5'] = 0;
	$pValores['5_5_6'] = 0;
	$pValores['5_5_' . $Valores['5_5']] = 1;

	$pValores['5_6_1'] = 0;
	$pValores['5_6_2'] = 0;
	$pValores['5_6_3'] = 0;
	$pValores['5_6_4'] = 0;
	$pValores['5_6_5'] = 0;
	$pValores['5_6_6'] = 0;
	$pValores['5_6_7'] = 0;
	$pValores['5_6_' . $Valores['5_6']] = 1;

	$pValores['5_7_1'] = 0;
	$pValores['5_7_2'] = 0;
	$pValores['5_7_3'] = 0;
	$pValores['5_7_4'] = 0;
	$pValores['5_7_' . $Valores['5_7']] = 1;

	$pValores['5_8_1'] = 0;
	$pValores['5_8_2'] = 0;
	$pValores['5_8_3'] = 0;
	$pValores['5_8_' . $Valores['5_8']] = 1;

	$pValores['5_9A_1'] = 0;
	$pValores['5_9A_2'] = 0;
	$pValores['5_9A_3'] = 0;
	$pValores['5_9A_4'] = 0;
	$pValores['5_9A_5'] = 0;
	$pValores['5_9A_6'] = 0;
	$pValores['5_9A_' . $Valores['5_9A']] = 1;

	$pValores['5_9B_1'] = 0;
	$pValores['5_9B_2'] = 0;
	$pValores['5_9B_3'] = 0;
	$pValores['5_9B_4'] = 0;
	$pValores['5_9B_5'] = 0;
	$pValores['5_9B_6'] = 0;
	$pValores['5_9B_' . $Valores['5_9B']] = 1;

	$pValores['5_9C_1'] = 0;
	$pValores['5_9C_2'] = 0;
	$pValores['5_9C_3'] = 0;
	$pValores['5_9C_4'] = 0;
	$pValores['5_9C_5'] = 0;
	$pValores['5_9C_6'] = 0;
	$pValores['5_9C_' . $Valores['5_9C']] = 1;

	$pValores['6_1_1'] = 0;
	$pValores['6_1_2'] = 0;
	if (isset($Valores['6_1']))
	{
		$pValores['6_1_' . $Valores['6_1']] = 1;
	}


$pdf = new FPDF();
$pdf->AddPage();

$pdf->SetFillColor(229, 236, 255);

$pdf->SetFont('Arial','B',8);
$pdf->MultiCell(20,4.68,'WSP COLOMBIA SAS',1,'C');
$pdf->SetXY(30,10);
$pdf->Cell(135,4,'FORMATO ENCUESTA DE CALIDAD',1,0,'C', 1);

$pdf->SetFont('Arial','B',6);
$pdf->MultiCell(35,4,"WSP  \n 17-Feb-15",1,'L');
$pdf->SetXY(165,18)	;

$pdf->SetFont('Arial','B',4);
$pdf->MultiCell(15,3,"TIPO DE VERIFICACI�N",1,'L');

$pdf->SetXY(180,18)	;
$pdf->Cell(10,3,'1',1,0,'C');
$pdf->Cell(10,3,'VISITA',1,0,'C');
$pdf->SetXY(180,21)	;
$pdf->Cell(10,3,'2',1,0,'C');
$pdf->Cell(10,3,'LLAMADA',1,0,'C');

$pdf->SetXY(30,14);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(135,4,'CONTRATO DE INTERVENTOR�A No. 0582',1,0,'C', 1);

$pdf->SetXY(30,18);

$pdf->Cell(135,6,'PROYECTO HOGARES DIGITALES',1,0,'C', 1);

$pdf->SetXY(10,24);
$pdf->SetFont('Arial','B',5);
$pdf->MultiCell(190,3,"La presente encuesta busca verificar la correcta prestaci�n del Servicio y su Nivel de Satisfacci�n del Usuario e Impacto social.\nS�lo para el encuestador: Saludo(Buenos d�as, tardes... Mi nombre es... Lo estamos visitando del Programa Hogares Digitales del Ministerio de Tecnolog�as de la Informaci�n y las Comunicaciones. Esta vivienda aparece como usuario de este Programa con el Operador (Leer el Nombre del Operador). En esta Ocasi�n vamos a evaluar la Calidad y los resultados del servicio que recibe).",1,'L');

$pdf->SetXY(10,33);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,4,'0. MODULO DE CONTROL DE LA ENCUESTA',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);

$pdf->SetFont('Arial','',6);
$pdf->Cell(40,4,'0.1 Fecha de Visita',1,0,'L');
$pdf->Cell(20,4,'DIA ' . substr($Valores['fechaInicio'], 8, 2) ,1,0,'L');
$pdf->Cell(20,4,'MES ' . substr($Valores['fechaInicio'], 5, 2),1,0,'L');
$pdf->Cell(20,4,'A�O ' . substr($Valores['fechaInicio'], 0, 4),1,0,'L');
$pdf->Cell(10,4,'',1,0,'L');
$pdf->Cell(28,4,'0.2 Hora de Visita',1,0,'L');
$pdf->Cell(28,4, substr($Valores['fechaInicio'], 10),1,0,'L');
$pdf->Cell(12,4,'AM',1,0,'L', $pValores['AM']);
$pdf->Cell(12,4,'PM',1,1,'L', $pValores['PM']);

$pdf->Cell(95,4,'0.3 Departamento ' . $Valores['Departamento'],1,0,'L');
$pdf->Cell(95,4,'0.3 Municipio ' . $Valores['NomMunicipio'],1,1,'L');
$pdf->Cell(190,4,'0.4 CODIGOS DE RESULTADO',1,1,'L');
$pdf->Cell(43,4,'Completa',1,0,'L');
$pdf->Cell(21,4,'1',1,0,'L', 1);
$pdf->Cell(42,4,'Incompleta',1,0,'L');
$pdf->Cell(21,4,'2',1,0,'L');
$pdf->Cell(42,4,'Rechazo',1,0,'L');
$pdf->Cell(21,4,'3',1,1,'L');

$pdf->Cell(30,4,'0.5 Entrevistador',1,0,'L');
$pdf->Cell(65,4,$Valores['Usuario'],1,0,'L');
$pdf->Cell(30,4,'0.6 Supervisor',1,0,'L');
$pdf->Cell(65,4,'',1,1,'L');

$pdf->Cell(30,4,'No Identificaci�n',1,0,'L');
$pdf->Cell(65,4,'',1,0,'L');
$pdf->Cell(30,4,'No Identificaci�n',1,0,'L');
$pdf->Cell(65,4,'',1,1,'L');

$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'1. ASPECTOS GENERALES',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);

$pdf->SetFont('Arial','',6);
$pdf->Cell(85,8,'1.1 CONTRATISTA',1,0,'L');
$pdf->Cell(30,8,'Contrato de Aporte',1,1,'L');
$pdf->Cell(85,4,'TELMEX DE COLOMBIA S.A',1,0,'L');
$pdf->Cell(30,4,'518',1,1,'L', $pValores[518]);
$pdf->Cell(85,4,'EMPRESA TELECOMUNICACIONES DE BUCARAMANGA S.A ESP',1,0,'L');
$pdf->Cell(30,4,'519',1,1,'L', $pValores[519]);
$pdf->Cell(85,4,'COLOMBIA TELECOMUNICACION S.A ESP',1,0,'L');
$pdf->Cell(30,4,'520',1,1,'L', $pValores[520]);

$pdf->SetXY(125,65);
$pdf->Cell(25,20,'1.2 No. Contrato',1,0,'L');
$pdf->Cell(50,20,'',1,0,'L');

$pdf->SetXY(10,85);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'2. DATOS DEL USUARIO BENEFICIARIO',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);

$pdf->SetFont('Arial','',6);
$pdf->Cell(45,4,'2.1 Nombre del Beneficiario',1,0,'L');
$pdf->Cell(145,4,$Valores['BeneficiarioNombre'],1,1,'L');
$pdf->Cell(45,4,'2.2 Documento de Identidad',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioDocumento'],1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(45,4,'2.3 ID. Beneficiario',1,0,'L');
$pdf->Cell(45,4,$Valores['idBeneficiario'],1,1,'L');
$pdf->Cell(45,4,'2.4 Direcci�n',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioDireccion'],1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(45,4,'2.5 Barrio',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioBarrio'],1,1,'L');
$pdf->Cell(45,4,'2.6 Telefono',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioTelefono'],1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(45,4,'2.7 Celular',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioCelular'],1,1,'L');
$pdf->Cell(45,4,'2.8 Municipio',1,0,'L');
$pdf->Cell(45,4,$Valores['NomMunicipio'],1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(45,4,'2.9 Departamento',1,0,'L');
$pdf->Cell(45,4,$Valores['Departamento'],1,1,'L');
$pdf->Cell(45,4,'2.10 Estrato',1,0,'L');
$pdf->Cell(45,4,$Valores['BeneficiarioEstrato'],1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(45,4,'',1,0,'L');
$pdf->Cell(45,4,'',1,1,'L');
$pdf->Cell(45,8,'2.11 Correo Electr�nico',1,0,'L');
$pdf->Cell(15,8,'Tiene',1,0,'L');
$pdf->Cell(15,4,'Si',1,1,'L', $pValores['CorreoSI']);
$pdf->SetX(70);
$pdf->Cell(15,4,'No',1,1,'L', $pValores['CorreoNO']);
$pdf->SetXY(85, 113);
$pdf->Cell(15,8,'CUAL?',1,0,'L');
$pdf->Cell(100,8,$Valores['BeneficiarioCorreo'],1,1,'L');
$pdf->Cell(45,8,'2.12 Genero',1,0,'L');
$pdf->Cell(15,4,'FEMENINO',1,0,'L');
$pdf->Cell(15,4,'',1,1,'L');
$pdf->SetX(55);
$pdf->Cell(15,4,'MASCULINO',1,0,'L');
$pdf->Cell(15,4,'',1,1,'L');

$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'3. DATOS PERSONA QUE ATIENDE LA ENCUESTA',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);

$pdf->SetFont('Arial','',6);
$pdf->Cell(45,4,'3.1 Nombre',1,0,'L');
$pdf->Cell(145,4,$Valores['3_1'],1,1,'L');
$pdf->Cell(45,4,'3.2 Parentesco',1,0,'L');
$pdf->Cell(10,4,'',0,0,'L');
$pdf->Cell(25,8,'3.3 Correo Electr�nico',1,0,'L');
$pdf->Cell(15,8,'TIENE',1,0,'L');
$pdf->Cell(15,4,'Si 1',1,1,'L', $pValores['3_CorreoSI']);
$pdf->SetX(105);
$pdf->Cell(15,4,'No 2',1,1,'L', $pValores['3_CorreoNO']);
$pdf->SetXY(120, 137);
$pdf->Cell(10,8,'CUAL?',1,0,'L');
$pdf->Cell(70,8,$Valores['3_3'],1,1,'L');
$pdf->SetXY(10, 141);
$pdf->Cell(35,4,"\t\t\t\t	Conyugue",1,0,'L');
$pdf->Cell(10,4,"1",1,1,'L', $pValores['3_2_1']);
$pdf->Cell(35,4,"\t\t\t\t	Hijo(a)",1,0,'L');
$pdf->Cell(10,4,"2",1,1,'L', $pValores['3_2_2']);
$pdf->Cell(35,4,"\t\t\t\t	Familiar",1,0,'L');
$pdf->Cell(10,4,"3",1,1,'L', $pValores['3_2_3']);
$pdf->Cell(35,4,"\t\t\t\t	Amigo",1,0,'L');
$pdf->Cell(10,4,"4",1,1,'L', $pValores['3_2_4']);
$pdf->Cell(35,4,"\t\t\t\t	Ninguno",1,0,'L');
$pdf->Cell(10,4,"5",1,1,'L', $pValores['3_2_5']);

$pdf->SetXY(65, 149);
$pdf->Cell(30,4,'3.4 Genero',1,1,'L');
$pdf->SetX(65);
$pdf->Cell(15,4,'FEMENINO',1,0,'L');
$pdf->Cell(15,4,'MASCULINO',1,1,'L');
$pdf->SetX(65);
$pdf->Cell(15,4,'',1,0,'L', $pValores['3_4_1']);
$pdf->Cell(15,4,'',1,1,'L', $pValores['3_4_2']);

$pdf->Cell(30,24,'3.6 Edad',1,0,'L');
$pdf->Cell(30,4,'Menor de 18 a�os',1,0,'L');
$pdf->Cell(10,4,'1',1,1,'L', $pValores['3_6_1']);
$pdf->SetX(40);
$pdf->Cell(30,4,'Entre 18 y 26 a�os',1,0,'L');
$pdf->Cell(10,4,'2',1,1,'L', $pValores['3_6_2']);
$pdf->SetX(40);
$pdf->Cell(30,4,'Entre 27 y 30 a�os',1,0,'L');
$pdf->Cell(10,4,'3',1,1,'L', $pValores['3_6_3']);
$pdf->SetX(40);
$pdf->Cell(30,4,'Entre 31 y 40 a�os',1,0,'L');
$pdf->Cell(10,4,'4',1,1,'L', $pValores['3_6_4']);
$pdf->SetX(40);
$pdf->Cell(30,4,'Entre 41 y 50 a�os',1,0,'L');
$pdf->Cell(10,4,'5',1,1,'L', $pValores['3_6_5']);
$pdf->SetX(40);
$pdf->Cell(30,4,'51 o mas a�os',1,0,'L');
$pdf->Cell(10,4,'6',1,1,'L', $pValores['3_6_6']);
$pdf->SetXY(82, 161);

$pdf->Cell(30,24,'3.7 Nivel Educativo Alcanzado',1,0,'L');
$pdf->Cell(30,4,'Primaria',1,0,'L');
$pdf->Cell(10,4,'1',1,1,'L', $pValores['3_7_1']);
$pdf->SetX(112);
$pdf->Cell(30,4,'Secundaria',1,0,'L');
$pdf->Cell(10,4,'2',1,1,'L', $pValores['3_7_2']);
$pdf->SetX(112);
$pdf->Cell(30,4,'T�cnico/Tecnologo',1,0,'L');
$pdf->Cell(10,4,'3',1,1,'L', $pValores['3_7_3']);
$pdf->SetX(112);
$pdf->Cell(30,4,'Profesional',1,0,'L');
$pdf->Cell(10,4,'4',1,1,'L', $pValores['3_7_4']);
$pdf->SetX(112);
$pdf->Cell(30,4,'Postgrado',1,0,'L');
$pdf->Cell(10,4,'5',1,1,'L', $pValores['3_7_5']);
$pdf->SetX(112);
$pdf->Cell(30,4,'Ninguno',1,0,'L');
$pdf->Cell(10,4,'6',1,1,'L', $pValores['3_7_6']);

$pdf->SetXY(160, 161);

$pdf->Cell(40,4,'3.8 Es usted cabeza de Hogar?',1,1,'L');
$pdf->SetXY(160, 165);
$pdf->Cell(20,4,'SI',1,0,'C', $pValores['3_8_1']);
$pdf->Cell(20,4,'NO',1,0,'C', $pValores['3_8_2']);
$pdf->SetXY(10, 185);

$pdf->Cell(45,4,'3.9 Cu�l es su Ocupaci�n Actual?',1,1,'C');
$pdf->Cell(35,4,"\t\t\t\t	Empleado",1,0,'L');
$pdf->Cell(10,4,"1",1,1,'L', $pValores['3_9_1']);
$pdf->Cell(35,4,"\t\t\t\t	Trabajador Independiente",1,0,'L');
$pdf->Cell(10,4,"2",1,1,'L', $pValores['3_9_2']);
$pdf->Cell(35,4,"\t\t\t\t	Estudiante",1,0,'L');
$pdf->Cell(10,4,"3",1,1,'L', $pValores['3_9_3']);
$pdf->Cell(35,4,"\t\t\t\t	Oficios del Hogar",1,0,'L');
$pdf->Cell(10,4,"4",1,1,'L', $pValores['3_9_4']);
$pdf->Cell(35,4,"\t\t\t\t	Jubilado/Pensionado",1,0,'L');
$pdf->Cell(10,4,"5",1,1,'L', $pValores['3_9_5']);
$pdf->Cell(35,4,"\t\t\t\t	No Trabaja",1,0,'L');
$pdf->Cell(10,4,"6",1,1,'L', $pValores['3_9_6']);
$pdf->Cell(35,4,"\t\t\t\t	NS/NR",1,0,'L');
$pdf->Cell(10,4,"7",1,1,'L', $pValores['3_9_7']);


$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'4. VERIFICACI�N EFECTOS SOCIALES Y SATISFACCI�N DEL USUARIO - NSU',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);

$pdf->SetFont('Arial','',6);

$pdf->Cell(56,4,'4.1 Conoce usted el programa Hogares Digitales?',1,0,'L');
$pdf->Cell(13,4,'SI	1',1,0,'C', $pValores['4_1_1']);
$pdf->Cell(13,4,'NO	2',1,0,'C', $pValores['4_1_2']);
$pdf->Cell(13,4,'NS/NR	3',1,1,'C', $pValores['4_1_3']);

$pdf->Cell(95,4,'4.2 Cree usted que el acceso a Internet por medio del Programa HD le ha permitido:',1,1,'L');
$pdf->Cell(82,4,"\t	a. Mejorar el rendimiento Acad�mico (de los estudiantes del hogar)",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_2_1']);
$pdf->Cell(82,4,"\t	b. Mejorar las condiciones de comercializaci�n y Producci�n (si tiene negocio)",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_2_2']);
$pdf->Cell(82,4,"\t	c. Mejorar condiciones laborales (si depende del Internet para actividades laborales)",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_2_3']);
$pdf->Cell(82,4,"\t	d. Ahorro en tiempo y dinero en actividades frecuentes del hogar",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_2_4']);
$pdf->Cell(82,4,"\t	e. NS/NR",1,0,'L');
$pdf->Cell(13,4,'5',1,1,'L', $pValores['4_2_5']);

$pdf->SetXY(110, 225);
$pdf->Cell(90,4,'4.3 Considera que actualmente el Programa Hogares Digitales es:',1,1,'L');
$pdf->SetX(110);
$pdf->Cell(77,4,"\t	a. Importante en el Hogar",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_3_1']);
$pdf->SetX(110);
$pdf->Cell(77,4,"\t	b. Algo Costoso",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_3_2']);
$pdf->SetX(110);
$pdf->Cell(77,4,"\t	c. Innecesario",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_3_3']);
$pdf->SetX(110);
$pdf->Cell(77,4,"\t	d. Ajustado a las necesidades",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_3_4']);
$pdf->SetX(110);
$pdf->Cell(77,4,"\t	e. NS/NR",1,0,'L');
$pdf->Cell(13,4,'5',1,1,'L', $pValores['4_3_5']);

$pdf->SetY(250);

$pdf->MultiCell(95,3,'4.4 Con el acceso a Internet a trav�s del Programa Hogares Digitales, considera que las relaciones con amigos y familiares ha:',1,'L');
$pdf->Cell(82,4,"\t	a. Mejorado, porque me conecto con ellos con m�s frecuencia",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_4_1']);
$pdf->Cell(82,4,"\t	b. Se ha mantenido Igual",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_4_2']);
$pdf->Cell(82,4,"\t	c. Ha desmejorado",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_4_3']);
$pdf->Cell(82,4,"\t	d. No utilizo Internet para comunicarme con amigos y familiares",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_4_4']);
$pdf->Cell(82,4,"\t	e. NS/NR",1,0,'L');
$pdf->Cell(13,4,'5',1,1,'L', $pValores['4_4_5']);

$pdf->SetXY(110, 250);

$pdf->MultiCell(90,3,'4.5 El servicio de Internet del Programa Hogares Digitales, le ha permitido la opotunidad de realizar actividades comerciales, laborales u otras actividades econ�micas?',1,'L');
$pdf->SetX(110);
$pdf->Cell(30,4,"\t	SI\t1",1,0,'L', $pValores['4_5_1']);
$pdf->Cell(30,4,"\t	NO\t2",1,0,'L', $pValores['4_5_2']);
$pdf->Cell(30,4,"\t	NS/NR\t3",1,0,'L', $pValores['4_5_3']);

$pdf->AddPage();

$pdf->MultiCell(60,3,'4.6 Con qu� frecuencia (en d�as usted usa ell servicio de Internet?',1,'L');
$pdf->Cell(47,4,"\t	a. Diariamente",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_6_1']);
$pdf->Cell(47,4,"\t	b. Una vez a la semana",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_6_2']);
$pdf->Cell(47,4,"\t	c. Mas de una vez a la semana",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_6_3']);
$pdf->Cell(47,4,"\t	d. Ocasionalmente",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_6_4']);
$pdf->Cell(47,4,"\t	e. Nunca",1,0,'L');
$pdf->Cell(13,4,'5',1,1,'L', $pValores['4_6_5']);
$pdf->Cell(47,4,"\t	f. NS/NR",1,0,'L');
$pdf->Cell(13,4,'6',1,1,'L', $pValores['4_6_6']);

$pdf->SetXY(75, 10);
$pdf->MultiCell(60,3,'4.7 Cuanto tiempo permanece conectado al servicio de Internet?',1,'L');
$pdf->SetX(75);
$pdf->Cell(47,4,"\t	a. Menos de una hora",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_7_1']);
$pdf->SetX(75);
$pdf->Cell(47,4,"\t	b. Entre 1 a 3 horas",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_7_2']);
$pdf->SetX(75);
$pdf->Cell(47,4,"\t	c. Mas de 3 horas",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_7_3']);
$pdf->SetX(75);
$pdf->Cell(47,4,"\t	d. NS/NR",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_7_4']);

$pdf->SetXY(140, 10);
$pdf->MultiCell(60,3,'4.8 El servicio de Internet del Programa Hogares Digitales, le ha permitido la opotunidad de realizar actividades comerciales, laborales u otras actividades econ�micas?',1,'L');
$pdf->SetX(140);
$pdf->Cell(47,4,"\t	a. Diurno",1,0,'L');
$pdf->Cell(13,4,'1',1,1,'L', $pValores['4_8_1']);
$pdf->SetX(140);
$pdf->Cell(47,4,"\t	b. Nocturno",1,0,'L');
$pdf->Cell(13,4,'2',1,1,'L', $pValores['4_8_2']);
$pdf->SetX(140);
$pdf->Cell(47,4,"\t	c. Sin Preferencia",1,0,'L');
$pdf->Cell(13,4,'3',1,1,'L', $pValores['4_8_3']);
$pdf->SetX(140);
$pdf->Cell(47,4,"\t	d. NS/NR",1,0,'L');
$pdf->Cell(13,4,'4',1,1,'L', $pValores['4_8_4']);

$pdf->SetXY(10, 41);
$pdf->Cell(93,4,"4.9 Utiliza Cabinas para uso del Internet?",1,1,'L');
$pdf->Cell(31,4,"SI 1",1,0,'L', $pValores['4_9_1']);
$pdf->Cell(31,4,"No 1",1,0,'L', $pValores['4_9_2']);
$pdf->Cell(31,4,"NS/NR 3",1,1,'L', $pValores['4_9_3']);

$pdf->SetXY(110, 41);
$pdf->MultiCell(90,3,'4.10 Por qu� Prefiere su Uso?',1,'L');
$pdf->SetX(110);
$pdf->Cell(37,4,"\t	a. Costo",1,0,'L');
$pdf->Cell(8,4,'1',1,0,'L', $pValores['4_10_1']);
$pdf->Cell(37,4,"\t	b. Velocidad",1,0,'L');
$pdf->Cell(8,4,'2',1,1,'L', $pValores['4_10_2']);
$pdf->SetX(110);
$pdf->Cell(37,4,"\t	c. Mejor Servicio (aporo para su uso)",1,0,'L');
$pdf->Cell(8,4,'3',1,0,'L', $pValores['4_10_3']);
$pdf->Cell(37,4,"\t	d. NS/NR",1,0,'L');
$pdf->Cell(8,4,'4',1,1,'L', $pValores['4_10_4']);

$pdf->SetY(53);
$pdf->MultiCell(190,4,'4.11 Califique los siguientes items de actividades de acuerdo a la frecuencia de utilizaci�n, teniendo en cuenta que 1 es Nunca y 5 Muy Frecuente',1,'L');
$pdf->Cell(115,4,"",1,0,'L');
$pdf->Cell(15,4,'Nunca',1,0,'C');
$pdf->Cell(15,4,'Casi Nunca',1,0,'C');
$pdf->Cell(15,4,'Normalmente',1,0,'C');
$pdf->Cell(15,4,'Frecuente',1,0,'C');
$pdf->Cell(15,4,'Muy Frecuente',1,1,'C');

$pdf->Cell(115,4,"\t a. Chat/VideoChat/Redes Sociales",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11A_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11A_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11A_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11A_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11A_5']);
$pdf->Cell(115,4,"\t b. Transacciones/Pagos",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11B_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11B_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11B_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11B_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11B_5']);
$pdf->Cell(115,4,"\t c. Por actividades Laborales/Comerciales",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11C_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11C_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11C_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11C_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11C_5']);
$pdf->Cell(115,4,"\t d. Entretenimiento",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11D_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11D_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11D_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11D_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11D_5']);
$pdf->Cell(115,4,"\t e. Investigaci�n/Tareas/Estudio/Noticias",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11E_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11E_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11E_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11E_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11E_5']);
$pdf->Cell(115,4,"\t f. Otros",1,0,'L');
$pdf->Cell(15,4,'1',1,0,'C', $pValores['4_11F_1']);
$pdf->Cell(15,4,'2',1,0,'C', $pValores['4_11F_2']);
$pdf->Cell(15,4,'3',1,0,'C', $pValores['4_11F_3']);
$pdf->Cell(15,4,'4',1,0,'C', $pValores['4_11F_4']);
$pdf->Cell(15,4,'5',1,1,'C', $pValores['4_11F_5']);



$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'5. PRESTACI�N DEL SERVICIO',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);
$pdf->SetFont('Arial','',6);

$pdf->Cell(131,4,"5.1 Sabe donde reportar las fallas del servicio de Internet Banda Ancha?",1,0,'L');
$pdf->Cell(20,4,'',0,0,'C');
$pdf->Cell(13,4,'SI 1',1,0,'C', $pValores['5_1_1']);
$pdf->Cell(13,4,'NO 2',1,0,'C', $pValores['5_1_2']);
$pdf->Cell(13,4,'NS/NR 3',1,1,'C', $pValores['5_1_3']);

$pdf->Cell(131,4,"5.2 Cuando realiza el reporte de fallas del servicio de Internet Banda Ancha; estas han sido atendidas?",1,0,'L');
$pdf->Cell(20,4,'',0,0,'C');
$pdf->Cell(13,4,'SI 1',1,0,'C', $pValores['5_2_1']);
$pdf->Cell(13,4,'NO 2',1,0,'C', $pValores['5_2_2']);
$pdf->Cell(13,4,'NS/NR 3',1,1,'C', $pValores['5_2_3']);

$pdf->Cell(131,4,"5.3 El servicio de Internet ha presentado fallas en el �ltimo mes?",1,0,'L');
$pdf->Cell(20,4,'',0,0,'C');
$pdf->Cell(13,4,'SI 1',1,0,'C', $pValores['5_3_1']);
$pdf->Cell(13,4,'NO 2',1,0,'C', $pValores['5_3_2']);
$pdf->Cell(13,4,'NS/NR 3',1,1,'C', $pValores['5_3_3']);

$pdf->Cell(131,4,"5.4 Usted report� fallas presentadas en el �ltimo mes?",1,0,'L');
$pdf->Cell(20,4,'',0,0,'C');
$pdf->Cell(13,4,'SI 1',1,0,'C', $pValores['5_4_1']);
$pdf->Cell(13,4,'NO 2',1,0,'C', $pValores['5_4_2']);
$pdf->Cell(13,4,'NS/NR 3',1,1,'C', $pValores['5_4_3']);

$pdf->SetXY(10, 106);
$pdf->Cell(60,24,"5.5 Cuantas fallas report� en el �ltimo mes",1,0,'L');
$pdf->Cell(30,4,'a. Una',1,0,'L');
$pdf->Cell(10,4,'1',1,1,'C', $pValores['5_5_1']);
$pdf->SetX(70);
$pdf->Cell(30,4,'b. Dos',1,0,'L');
$pdf->Cell(10,4,'2',1,1,'C', $pValores['5_5_2']);
$pdf->SetX(70);
$pdf->Cell(30,4,'c. Tres',1,0,'L');
$pdf->Cell(10,4,'3',1,1,'C', $pValores['5_5_3']);
$pdf->SetX(70);
$pdf->Cell(30,4,'d. Cuatro',1,0,'L');
$pdf->Cell(10,4,'4',1,1,'C', $pValores['5_5_4']);
$pdf->SetX(70);
$pdf->Cell(30,4,'e. Mas de Cuatro',1,0,'L');
$pdf->Cell(10,4,'5',1,1,'C', $pValores['5_5_5']);
$pdf->SetX(70);
$pdf->Cell(30,4,'f. Ninguna',1,0,'L');
$pdf->Cell(10,4,'6',1,1,'C', $pValores['5_5_6']);

$pdf->SetXY(115, 106);
$pdf->MultiCell(85,4,'5.7 Cual fue el tipo de falla que present� el servicio?',1,'L');
$pdf->SetX(115);
$pdf->Cell(75,4,"\ta. Fallas para conectarse",1,0,'L');
$pdf->Cell(10,4,'1',1,1,'C', $pValores['5_7_1']);
$pdf->SetX(115);
$pdf->Cell(75,4,"\tb. El Internet es muy lento",1,0,'L');
$pdf->Cell(10,4,'2',1,1,'C', $pValores['5_7_2']);
$pdf->SetX(115);
$pdf->Cell(75,4,"\tc. Se cae frecuentemente",1,0,'L');
$pdf->Cell(10,4,'3',1,1,'C', $pValores['5_7_3']);
$pdf->SetX(115);
$pdf->Cell(75,4,"\td. NS/NR",1,0,'L');
$pdf->Cell(10,4,'4',1,1,'C', $pValores['5_7_4']);

$pdf->SetXY(10, 131);
$pdf->Cell(60,28,"5.6 Cuanto Tiempo transcurri� para ser atendida la falla?",1,0,'L');
$pdf->Cell(30,4,'a. Un D�a',1,0,'L');
$pdf->Cell(10,4,'1',1,1,'C', $pValores['5_6_1']);
$pdf->SetX(70);
$pdf->Cell(30,4,'b. 2 a 3 D�as',1,0,'L');
$pdf->Cell(10,4,'2',1,1,'C', $pValores['5_6_2']);
$pdf->SetX(70);
$pdf->Cell(30,4,'c. 4 a 5 D�as',1,0,'L');
$pdf->Cell(10,4,'3',1,1,'C', $pValores['5_6_3']);
$pdf->SetX(70);
$pdf->Cell(30,4,'d. Una Semana',1,0,'L');
$pdf->Cell(10,4,'4',1,1,'C', $pValores['5_6_4']);
$pdf->SetX(70);
$pdf->Cell(30,4,'e. Mas de una Semana',1,0,'L');
$pdf->Cell(10,4,'5',1,1,'C', $pValores['5_6_5']);
$pdf->SetX(70);
$pdf->Cell(30,4,'f. Mas de 1 mes',1,0,'L');
$pdf->Cell(10,4,'6',1,1,'C', $pValores['5_6_6']);
$pdf->SetX(70);
$pdf->Cell(30,4,'g. NS/NR',1,0,'L');
$pdf->Cell(10,4,'7',1,1,'C', $pValores['5_6_7']);

$pdf->SetXY(116, 131);

$pdf->Cell(84,4,"5.8 Recibe su factura a tiempo (Puntualidad)?",1,1,'L');
$pdf->SetX(116);
$pdf->Cell(28,4,'SI 1',1,0,'L', $pValores['5_8_1']);
$pdf->Cell(28,4,'NO 2',1,0,'L', $pValores['5_8_2']);
$pdf->Cell(28,4,'NS/NR 3',1,1,'L', $pValores['5_8_3']);

$pdf->SetY(160);
$pdf->MultiCell(117,4,'5.9 Percepci�n de la Atenci�n al Usuario',1,'L');
$pdf->SetXY(127, 160);
$pdf->Cell(12,4,'Muy Malo',1,0,'C');
$pdf->Cell(12,4,'Malo',1,0,'C');
$pdf->Cell(12,4,'Normal',1,0,'C');
$pdf->Cell(12,4,'Bueno',1,0,'C');
$pdf->Cell(12,4,'Muy Bueno',1,0,'C');
$pdf->Cell(12,4,'No Hubo',1,1,'C');

$pdf->MultiCell(117,4,"\ta. Atenci�n recibida a reclamaciones que ha realizado por el servicio",1,'L');
$pdf->SetXY(127, 164);
$pdf->Cell(12,4,'1',1,0,'C', $pValores['5_9A_1']);
$pdf->Cell(12,4,'2',1,0,'C', $pValores['5_9A_2']);
$pdf->Cell(12,4,'3',1,0,'C', $pValores['5_9A_3']);
$pdf->Cell(12,4,'4',1,0,'C', $pValores['5_9A_4']);
$pdf->Cell(12,4,'5',1,0,'C', $pValores['5_9A_5']);
$pdf->Cell(12,4,'6',1,1,'C', $pValores['5_9A_6']);

$pdf->MultiCell(117,4,"\tb. Atenci�n que ha recibido del servicio a trav�s de las l�neas telef�nicas o el Centro de Atenci�n al Cliente",1,'L');
$pdf->SetXY(127, 168);
$pdf->Cell(12,4,'1',1,0,'C', $pValores['5_9B_1']);
$pdf->Cell(12,4,'2',1,0,'C', $pValores['5_9B_2']);
$pdf->Cell(12,4,'3',1,0,'C', $pValores['5_9B_3']);
$pdf->Cell(12,4,'4',1,0,'C', $pValores['5_9B_4']);
$pdf->Cell(12,4,'5',1,0,'C', $pValores['5_9B_5']);
$pdf->Cell(12,4,'6',1,1,'C', $pValores['5_9B_6']);

$pdf->MultiCell(117,4,"\tc. Disposici�n de las personas que atienden las solicitudes de servicio",1,'L');
$pdf->SetXY(127, 172);
$pdf->Cell(12,4,'1',1,0,'C', $pValores['5_9C_1']);
$pdf->Cell(12,4,'2',1,0,'C', $pValores['5_9C_2']);
$pdf->Cell(12,4,'3',1,0,'C', $pValores['5_9C_3']);
$pdf->Cell(12,4,'4',1,0,'C', $pValores['5_9C_4']);
$pdf->Cell(12,4,'5',1,0,'C', $pValores['5_9C_5']);
$pdf->Cell(12,4,'6',1,1,'C', $pValores['5_9C_6']);



$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 236, 255);
$pdf->Cell(190,4,'6. DATOS Y FIRMAS DE LA ENCUESTA',1,1,'C', 1);
$pdf->SetFillColor(115, 115, 115);
$pdf->SetFont('Arial','',6);

$pdf->MultiCell(150,4,"Autoriza toma de Fotograf�a de la fachada del predio, donde se observe claramente la direcci�n de vivienda?",1,'L');
$pdf->SetXY(160, 180);
$pdf->Cell(20,4,'SI 1',1,0,'L', $pValores['6_1_1']);
$pdf->Cell(20,4,'NO 2',1,1,'L', $pValores['6_1_2']);
$pdf->Cell(190,4,'En Caso NEGATIVO por favor firmar',0,1,'L');

$pdf->SetY(190);

$pdf->SetFont('Arial','B',6);
$pdf->Cell(95,4,'SUPERVISOR POR PARTE DE LA INTERVENTOR�A',0,0,'L');
$pdf->Cell(95,4,'PERSONA QUE ATENDI� LA VISITA',0,1,'L');
$pdf->Cell(95,4,'FIRMA',0,0,'L');
$pdf->Cell(95,4,'FIRMA',0,1,'L');
$pdf->Cell(95,4,'NOMBRE',0,0,'L');
$pdf->Cell(95,4,'NOMBRE',0,1,'L');
$pdf->Cell(95,4,'CEDULA',0,0,'L');
$pdf->Cell(95,4,'CEDULA',0,1,'L');

$pdf->Image("font/firmas/" . $Valores['Usuario']. '.png',10,190,90, 30);

$ruta = "../Archivos/files/Visitas/" . $Valores['Prefijo'];
$idx = 0;
$posX = 10;
$posY = 10;
if (is_dir($ruta)) 
{ 
  if ($dh = opendir($ruta)) 
  { 
     while (($file = readdir($dh)) !== false) 
     {
        if ( $file!="." && $file!=".." && (substr($file, -3) == "jpg" || substr($file, -3) == "png")) 
        {
        	if ($idx == 0)
        	{
        		$pdf->AddPage();
        	} 

        	if ($idx%2==0)
        	{
			    $posX = 10;
			    $tPosY = $posY;
			}else{
			    $posX = 115;
			    $tPosY = $posY + 92;
			}

           $pdf->Image("../Archivos/files/Visitas/" . $Valores['Prefijo'] . "/" . $file,$posX,$posY,90, 90);
           $posY = $tPosY;

        	$idx++;

           if ($posX == 115 && $tPosY > 220)
           {
           		$idx = 0;
           		$posX = 10;
				$posY = 10;
           }        	
        }
     } 
  closedir($dh); 

  } 
}

$pdf->Output();
?>
